{{--    Шаблон для редактирования статьи через панель управления    --}}

<article id="{{!$article ? null : $article->id}}">
	<div id="article_body">
		<div class="row">
			<div class="col-md-12">
				<h2>Заголовок</h2>
				<div class="form-group">
					<input type="text" name="title" placeholder="Основной заголовок статьи (не более 50 символов)" value="{{!$article ? null : $article->title}}" maxlength="50">
				</div>
			</div>
		</div>
		<h2>Описание</h2>
		<textarea name="article_description" rows="3" id="description">{{!$article ? null :$article->description}}</textarea>
		<div class="control-group">
			<label class="control control--checkbox">Новость
				<input id="is_news" type="checkbox" {{!$article ? null : $article->news ? 'checked' : ''}}>
				<div class="control__indicator"></div>
			</label>
		</div>

		<h3>Загрузить изображение</h3>
		<div class="thumbnails">
			<div class="img_add">
				<div class="file_upload label_red">
					<span class="icon-plus">
						<input type="file" name="img_main" class="upload" id="upload_thumb" data-project_id="{{!$article ? null :$article->id}}">
					</span>
				</div>
			</div>
		</div>
		@if(!$article)
			<button type="button" class="post"  data-target="articles/post"><div class="loader-inner ball-pulse">Сохранить</div></button>
		@else
			<button type="button" class="post" data-article_id="{{$article->id}}" data-target="articles/post"><div class="loader-inner ball-pulse">Сохранить</div></button>
			<button type="button" class="delete" data-article_id="{{$article->id}}" data-target="articles/delete"><div class="loader-inner ball-pulse">Удалить</div></button>
		@endif
	</div>
</article>