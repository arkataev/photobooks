@extends('admin.layout')
@section('content')
	<article class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="border_block">

					<div id="services">
						<h2>Услуги</h2>
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<ul>
								@foreach($services as $service)
									<li>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingOne">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#service_{{$service->id}}" aria-expanded="false" aria-controls="service_{{$service->id}}">
														<h3>{{$service->name}}</h3>
													</a>
												</h4>
											</div>
											<div id="service_{{$service->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
												<div class="panel-body">
													<h2>Описание</h2>
													<textarea class="service_description" name="service_description" rows="3" id="description_{{$service->id}}">{{$service->description}}</textarea>
													<button type="button" class="post service_update" id="{{$service->id}}"><div class="loader-inner ball-pulse">Сохранить</div></button>
												</div>
											</div>
										</div>
									</li>
								@endforeach
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>

	</article>
@endsection
@section('scripts')
	<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
	<script>CKEDITOR.replaceAll();</script>
@endsection