<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="ru" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="ru" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="ru" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('fonts/fontello/css/fontello.css') }}">
	<title>Фотокнига.com | Вход в панель управления</title>
</head>
<body>

<div id="login">
	<p>Привет, Хозяйка!</p>
	<form action="auth" method="post">
		<div class="form-group">
			<input type="text" name="username" id="username" placeholder="Логин">
		</div>
		<div class="form-group">
			<input type="password" name="password" placeholder="Пароль">
		</div>
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<button type="submit" class="btn btn-default">Войти</button>
	</form>
</div>
</body>