@extends('admin.layout')
@section('content')
	<article class="container">
		<div id="banners">

			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="border_block">
						<h2>Баннеры</h2>
						@foreach($banners as $banner)
							<div class="banner_body review_item item-lg" id="banner_{{$banner->id}}">
								<h3>1920 х 700</h3>
								<div class="row">
									<div class="col-md-12">
										<div class="img-thumbnail">
											<img class="img_main_preview" src="{{$banner->img_url}}" alt="">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" name="header_1" placeholder="Заголовок (не более 30 символов)" value="{{isset($banner->header_1) ? $banner->header_1 : null}}" maxlength="30">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" name="target_url" placeholder="Ссылка" value="{{isset($banner->target_url) ? $banner->target_url: null}}">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<input type="text" name="header_2" placeholder="Описание (не более 100 символов)" value="{{isset($banner->header_2) ? $banner->header_2 : null}}" maxlength="100">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="img_add">
											<div class="file_upload label_red">
										<span class="icon-plus">
											<input type="file" name="img_main" class="upload" id="upload_thumb" data-banner_id="{{$banner->id}}">
										</span>
											</div>
										</div>
									</div>
								</div>
								<button type="button" class="post banner_update" data-banner_id="{{$banner->id}}" data-target="banners/update"><div class="loader-inner ball-pulse">
										Обновить
									</div></button>
							</div>
						@endforeach


					</div>
				</div>
			</div>


		</div>
	</article>
@endsection