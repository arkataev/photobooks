@extends('admin.layout')
@section('content')
	<div id="img_modal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="thumbnail">
					<img id="image_crop" class="img_upload" src="" alt="">
					<button id="img_crop_set" class="btn btn-success img_crop" data-target="images/post">Сохранить</button>
				</div>
			</div>
		</div>
	</div>
	<article id="admin_gallery" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="gallery">
					<ul>
						@foreach($images as $image)
							<li id="{{$image->id}}">
								<button id='{{$image->id}}' class="btn btn-danger img_delete"><span class="icon-cancel"></span></button>
								<img src="{{asset('img/'. $image->route . '/image_' . $image->id . '_cr.jpg')}}" alt="">
							</li>
						@endforeach
							<li class="img_add">
								<div class="file_upload label_red">
									<span class="icon-plus">
										<input type="file" name="img_thumb" id="upload_thumb" class="upload" data-modal="img_upload">
									</span>
								</div>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</article>
@endsection
@section('scripts')
	<script src="{{asset('js/admin/jquery.imgareaselect.pack.js')}}"></script>
@endsection
