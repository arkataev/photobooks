@extends('admin.layout')
@section('content')
	<div id="article_modal" class="modal fade" tabindex="-2" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content"></div>
		</div>
	</div>
	<article class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="border_block">
						<h2>Статьи</h2>
						<button class="get_article" name="add_button" data-article=""><div class="loader-inner ball-pulse">Добавить</div></button>
						<ul id="admin_articles" class="modal_form">
							@foreach($articles as $article)
								<a href="#" class="get_article" data-article="{{$article->id}}">
									<li>
										@if($article->news)
											<p class="block_info_label label_black label_right label_small">Новость</p>
										@endif
										<div class="img_wrapper">
											<img src="{{asset('/img/articles/article_img_' . $article->id.'.jpg')}}" alt="">
										</div>
										<div class="project_desc_items">
											<h3>{{$article->title}}</h3>
										</div>
									</li>
								</a>
							@endforeach
						</ul>
					</div>
					{{ $articles->links() }}
				</div>
			</div>
	</article>
@endsection
@section('scripts')
	<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
@endsection