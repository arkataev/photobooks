<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="ru" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="ru" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="ru" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/loaders.css/loaders.min.css') }}">
	<link rel="stylesheet" href="{{ asset('fonts/fontello/css/fontello.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/imgareaselect-default.css') }}">
	<title>Фотокнига.com | Панель управления</title>
</head>
<body>
<div class="wrapper">
	<nav class="navbar navbar" role="navigation">
		<div class="container">
			<div class="main_nav" id="navbar_collapse">
				<li><a href="{{route('admin_banners')}}">Баннеры</a></li>
				<li><a href="{{route('admin_services')}}">Услуги</a></li>
				<li><a href="{{route('admin_projects')}}">Проекты</a></li>
				<li><a href="{{route('admin_articles')}}">Статьи</a></li>
				<li><a href="{{route('admin_reviews')}}">Отзывы</a></li>
				<li><a href="{{route('admin_gallery')}}">Галерея</a></li>
				<li><a href="{{route('logout')}}">Выход</a></li>
			</div>
		</div>
		<div class="navbar_bg"></div>
	</nav>
	<div id="alert-container" class="modal fade" tabindex="2" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="alert_error">
					<span id="message_type"></span>
					<ul class="message"></ul>
				</div>
			</div>
		</div>
	</div>
	@yield('content')
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
@yield('scripts')
<script type="text/javascript" src="{{ asset('/js/main.js')}} "></script>
<script type="text/javascript" src="{{asset('/js/admin/admin.js')}}"></script>
</body>
</html>