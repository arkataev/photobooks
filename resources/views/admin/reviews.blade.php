{{--  -------------------------------------------------------------
  --   Шаблон раздела Отзывы в панели управления
  --  -------------------------------------------------------------
  --
  -- Отображает список отзывов. Позволяет производить модерацию отзывов
  --
  --
  --}}
@extends('admin.layout')
@section('content')
	<article class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="border_block">
					<h2>Отзывы</h2>
					<ul class="reviews admin-reviews">
						@foreach($reviews as $review)
							<li class="review_item">
								<span class="block_info_label label_black label_right label_small">{{date('d/m/y', strtotime($review->date_add))}}</span>
								<div class="row">
									<div class="col-md-2">
										<div class="review_user_img">
											<img src="{{$review->user->vk_photo}}" alt="">
										</div>
									</div>
									<div class="col-md-10">
										<h3>{{$review->user->vk_full_name}}</h3>
										@if($review->approved)
											<span class="label label-success">Принят</span>
										@else
											<span class="label label-danger">Отклонен</span>
										@endif
										<p>{{$review->review_text}}</p>
									</div>
								</div>
								@if($review->approved)
									<button type="button" id="deny" class="delete" data-target="reviews/deny" data-review_id="{{$review->id}}"><div class="loader-inner ball-pulse">Отклонить</div></button>
								@else
									<button type="button" id="accept" class="post" data-target="reviews/approve" data-review_id="{{$review->id}}"><div class="loader-inner ball-pulse">Принять</div></button>
								@endif
							</li>
						@endforeach
					</ul>

				</div>
			</div>
		</div>

		{{ $reviews->links() }}
	</article>
@endsection