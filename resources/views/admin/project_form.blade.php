{{--    Шаблон формы редактирования/добавления проекта    --}}

<article id="{{!$project ? null : $project->id}}">
	<div id="project_body">
		{{--<div class="img-thumbnail">--}}
			{{--<img id="img_main_preview" src="{{!$project ? 'http://placehold.it/1900x1000' : asset('/img/projects/' . $project->id .'/image_' . $project->mainImage .'.jpg')}}" alt="">--}}
		{{--</div>--}}
		<div class="row">
			<div class="col-md-12">
				<h2>Название</h2>
				<div class="form-group">
					<input type="text" name="header_1" placeholder="Основной заголовок (не более 30 символов)" value="{{!$project ? null : $project->header_1}}" maxlength="30">
				</div>
				<h2>Краткое описание</h2>
				<div class="form-group">
					<input type="text" name="header_2" placeholder="Краткое описание (не более 100 символов)" value="{{!$project ? null : $project->header_2}}" maxlength="100">
				</div>
			</div>
		</div>
		<h2>Описание</h2>
		<textarea rows="3" id="description" name="project_description">{{!$project ? null : $project->description}}</textarea>
		<div class="category">
			<div class="category_select">
				<label for="category">Категория</label>
				<select id="category"  name="category">
						<option value="">Выберите</option>
						@if(!$project)
							@foreach($categories as $category)
								<option value="{{$category->id}}">{{$category->name}}</option>
							@endforeach
						@else
							@foreach($categories as $category)
								<option value="{{$category->id}}" {{$category->id == $project->category->id ? 'selected' : null}}>{{$category->name}}</option>
							@endforeach
						@endif
				</select>
			</div>
			<div class="category_select" id="project_type" style="{{$project ? null : 'display: none'}}">
				<label for="type">Вид</label>
				<select id="type" name="type">
					@if($project)
						@foreach($types as $type)
							<option value="{{$type->id}}" {{$type->id == $project->category_id ? 'selected' : null}}>{{$type->name}}</option>
						@endforeach
					@endif
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control control--checkbox">Избранное
				<input type="checkbox" id="featured" {{!$project ? null : $project->featured ? 'checked' : null}}>
				<div class="control__indicator"></div>
			</label>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="project_thumbs">
					<div class="row">
						<div class="col-md-12">
							<h3>Основное фото</h3>
							<ul class="main_thumb">
								<li class="img_add">
									<div class="file_upload label_red">
										<span class="icon-plus">
											<input type="file" name="img_main" class="upload" id="upload_thumb" data-project_id="{{!$project ? null : $project->id}}">
										</span>
									</div>
								</li>
								@if($project && $project->mainImage)
									<li>
										<button class="delete_thumb btn btn-danger" id="{{$project->mainImage}}" data-project="{{$project->id}}"><span class="icon-cancel"></span></button>
										<img class="edited" src="{{asset('/img/projects/' . $project->id .'/image_' . $project->mainImage.'_cr.jpg')}}" alt="">
									</li>
								@endif
							</ul>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h3>Дополнительные фото</h3>
							<ul class="thumbnails">
								@if($project && $project->images)
									@foreach($project->images as $image)
										@if(!$image->main)
											<li>
												<button class="delete_thumb btn btn-danger" id="{{$image->id}}" data-project="{{$project->id}}"><span class="icon-cancel"></span></button>
												<img class="edited" src="{{asset('img/projects/' . $project->id . '/image_' . $image->id .'_cr.jpg')}}" alt="">
											</li>
										@endif
									@endforeach
								@endif
								<li class="img_add">
									<div class="file_upload label_red">
										<span class="icon-plus">
											<input type="file" name="img_thumb" id="upload_thumbs" class="upload" data-project_id="" multiple>
										</span>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				@if(!$project)
					<button type="button" id="post_project" class="post" data-project_id="" data-target="projects/post" data-img_delete=""><div class="loader-inner ball-pulse">Сохранить</div></button>
				@else
					<button type="button" id="post_project" class="post" data-project_id="{{$project->id}}" data-target="projects/update" data-img_delete=""><div class="loader-inner ball-pulse">Сохранить</div></button>
					<button type="button" id="delete_project" class="delete" data-project_id="{{$project->id}}" data-target="projects/delete"><div class="loader-inner ball-pulse">Удалить</div></button>
				@endif
			</div>
		</div>
	</div>
</article>