{{--  -------------------------------------------------------------
  --   Шаблон раздела Проекты в панели управления
  --  -------------------------------------------------------------
  --
  -- Отображает список существующих проектов и позволяет добавлять новые проекты
  --
  --
  --}}

@extends('admin.layout')
@section('content')
	<div id="img_modal" class="modal fade" tabindex="2" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content modal-crop">
				<div class="thumbnail">
					<img id="image_crop" src="#" alt="Изображение для наложения обрезки">
				</div>
				<button id="img_crop_set" class="btn btn-success">Сохранить</button>
			</div>
		</div>
	</div>
	<div id="project_modal" class="modal fade" tabindex="-2" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content"></div>
		</div>
	</div>
	<article class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="border_block">
					<h2>Проекты</h2>
					<button class="get_project post" name="add_button" data-project=""><div class="loader-inner ball-pulse">Добавить</div></button>
					<ul id="projects" class="modal_form">
						@foreach($projects as $project)
							<a href="#" class="get_project" data-project="{{$project->id}}">
								<li>
									@if($project->featured)
										<p class="block_info_label label_black label_right label_small">Избранное</p>
									@endif
									<div class="img_wrapper">
										<img src="{{asset('/img/projects/' . $project->id .'/image_' . $project->mainImage .'_cr.jpg')}}" alt="">
									</div>
									<div class="project_desc_items">
										<h3>{{$project->header_1}}</h3>
										<p>{{$project->header_2}}</p>
										<span>#{{$project->category->name}}</span>
										<span>#{{$project->type->name}}</span>
									</div>
								</li>
							</a>
						@endforeach
					</ul>
				</div>
				{{ $projects->links() }}
			</div>
		</div>

	</article>
@endsection
@section('scripts')
	<script src="{{asset('js/admin/jquery.imgareaselect.pack.js')}}"></script>
	<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
@endsection