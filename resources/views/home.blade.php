@extends('layouts.layout')
@section('title')
	Школьные фотоальбомы | Фотосессии в Севастополе и Крыму
@endsection
@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="container mobile_head hidden-lg hidden-md hidden-sm">
				<a href="{{route('project', ['id'=> $featured->id])}}" class="project_thumb project_thumb_lg" id="{{$featured->id}}">
					<img src="{{asset('/img/projects/'. $featured->id.'/image_'. $featured->mainImage. '_cr.jpg')}}" alt="">
					<div class="mask"></div>
					<div class="tint tint_linear"></div>
					<div class="image_caption">
						<h2>Фотокниги& <br>Фотосессии</h2>
						<p>Школьные и персональные фотокниги в Севастополе и Крыму</p>
					</div>
				</a>
			</div>
			<div class="flexslider banner banner_lg hidden-xs" id="main_banner">
				<ul class="slides">
					@foreach($banners as $banner)
						<li class="slide">
							<div class="container caption_text">
								<div class="row">
									<div class="col-md-8 col-sm-12 col-xs-12">
										<h1>{{$banner->header_1}}</h1>
										<p>{{$banner->header_2}}</p>
										<a href="{{$banner->target_url}}" class="learn_more_btn">Подробнее</a>
									</div>
								</div>
							</div>
							<img src="{{$banner->img_url}}"/>
						</li>
					@endforeach
				</ul>
				<div class="tint tint_circular"></div>
			</div>
		</div>
	</div>
	<article class="container">
		<div class="block_info col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				@foreach($articles as $article)
				<div class="col-md-4 col-xs-12 col-sm-12">
					<div class="block_info_item">
						<div class="block_info_content">
							<h2>{{$article->title}}</h2>
							{!! str_limit($article->description, 200) !!}
						</div>
						<a href="{{route('article', ['id' => $article->id])}}" class="link">Подробнее</a>
					</div>
				</div>
				@endforeach
				<div class="col-md-4 col-xs-12 col-sm-12 last">
					<div class="block_info_item">
						@if( isset($news) )
						<div class="block_info_content">
							<h2>Новости</h2>
							<h3>{{str_limit($news->title, 40)}}</h3>
							<p>Опубликовано: {{date('d/m/y', strtotime($news->date_add))}}</p>
						</div>
						<a href="{{route('article', ['id' => $news->id])}}" class="link">Подробнее</a>
						@else
							<h3>Новостей пока нет...</h3>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="block_featured_projects">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12">
					<a href="{{route('project', ['id'=> $featured->id])}}" class="project_thumb project_thumb_lg hidden-sm hidden-xs" id="{{$featured->id}}">
						<span class="block_info_label label_large label_red label_left hidden-sm hidden-xs">Новые проекты</span>
						<img src="{{asset('/img/projects/'. $featured->id.'/image_'. $featured->mainImage. '_cr.jpg')}}" alt="">
						<div class="mask"></div>
						<div class="tint tint_linear"></div>
						<div class="image_caption">
							<h2>{{$featured->header_1}}</h2>
							<span>#{{$featured->category->name}}</span>
							<span>#{{$featured->type->name}}</span>
						</div>
					</a>@foreach($projects as $project)<a href="{{route('project', ['id'=> $project->id])}}" class="project_thumb project_thumb_sm" id="{{$project->id}}">
							<img src="{{asset('/img/projects/'. $project->id.'/image_'. $project->mainImage .'_cr.jpg')}}" alt="">
							<div class="mask"></div>
							<div class="tint tint_linear"></div>
							<div class="image_caption">
								<h2>{{$project->header_1}}</h2>
								<span>#{{$project->category->name}}</span>
								<span>#{{$project->type->name}}</span>
							</div>
						</a>@endforeach
				</div>
			</div>
		</div>
	</article>
	@if(!$reviews->isEmpty())
	<div class="reviews hidden-sm hidden-xs">
		<div class="container-fluid no-padding" >
			<div class="flexslider" id="reviews">
				<ul class="slides">
					@foreach($reviews as $review)
					<li>
						<a href="{{route('reviews')}}">
							<div class="review row" id="review_{{$review->id}}">
								<div class="col-md-4 review_img">
									<img class="review_ava" src="{{$review->user->vk_photo}}" alt="">
								</div>
								<div class="col-md-8">
									<p class="review_text">{{$review->review_text}}</p>
								</div>
							</div>
						</a>
					</li>@endforeach
				</ul>
			</div>
		</div>
	</div>
	@endif
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('/js/flex_slider/jquery.flexslider-min.js') }}"></script>
@endsection