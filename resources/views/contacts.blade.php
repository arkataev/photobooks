@extends('layouts.layout')
@section('title')
	Контакты
@endsection
@section('content')
	<div class="banner banner_sm" style="background-image: url('{{asset('img/banners/contacts.jpg')}}')">
		<div class="tint tint_circular"></div>
		<div class="container caption_text">
			<div class="row">
				<div class="col-md-8 col-md-offset-4">
					<h1>Контакты</h1>
				</div>
			</div>
		</div>
	</div>
	<article class="container">
		<div class="block_info">
			<div class="row">
				<div class="col-md-4">
					<div class="block_info_item">
						<div class="block_info_content">
							<p class="block_info_label label_black label_right label_small">9:00 - 18:00</p>
							<h2>Телефоны</h2>
							<ul>
								<li>+7 978 071 35 43 | Крым</li>
								<li>+7 911 250 00 03 | Санкт-Петербург</li>
								<li>+7 910 007 71 22 | Москва</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="block_info_item">
						<div class="block_info_content">
							<h2>Электропочта</h2>
							<ul>
								<li><a class="link" href="mailto:d.gargul@gmail.com">d.gargul@gmail.com</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4 last">
					<div class="block_info_item">
						<div class="block_info_content">
							<h2>Социальные сети</h2>
							<ul class="socials">
								<li><a href="https://vk.com/sevastopol.stage" class="link" target="_blank"><span class="icon-vkontakte">Вконтакте</span></a></li>
								<li><a href="https://www.instagram.com/daria_gargul" class="link" target="_blank"><span class="icon-instagram">Инстаграм</span></a></li>
								<li></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
@endsection