<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="ru" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="ru" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="ru" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('fonts/fontello/css/fontello.css') }}">
	<link rel="stylesheet" href="{{ asset('/js/flex_slider/flexslider.css') }}">
	<link rel="stylesheet" href="{{ asset('/js/fancybox/source/jquery.fancybox.css') }}">
	<title>Фотокнига.com | @yield('title')</title>
</head>
<body>
<div class="wrapper">
	<button id="navbar_show"><span class="icon-menu"></span></button>
	<nav class="navbar navbar-fixed-top" role="navigation">
		<div class="container">
			<ul class="main_nav" id="navbar_collapse">
				<li><a href="{{ route('home') }}">Фотокниги& <br/> Фотоальбомы</a></li>
				<li><a href="{{ route('home') }}">Главная</a></li>
				<li id="dd-toggle" class="dd-wrapper">
					<a href="{{route('services')}}">Работа</a>
					<ul id="dd-menu" class=" hidden-sm hidden-xs">
						<li><a href="{{ route('service', ['id' => 1]) }}">Фотоальбомы</a></li>
						<li><a href="{{ route('service', ['id' => 2]) }}">Фотосессии</a></li>
					</ul>
				</li>
				<li><a href="{{ route('projects') }}">Портфолио</a></li>
				<li><a href="{{ route('articles') }}">Новости</a></li>
				<li><a href="{{ route('reviews') }}">Отзывы</a></li>
				<li><a href="{{route('contacts')}}">Контакты</a></li>
			</ul>
		</div>
		<div class="navbar_bg hidden-sm hidden-xs"></div>
	</nav>
	@yield('content')
	<footer class="container-fluid no-padding">
		<div class="container main_footer">
			<div class="row">
				<div class="col-md-4">
					<h2>Контакты</h2>
					<ul>
						<li>+7 978 071 35 43 | Крым</li>
						<li>+7 911 250 00 03 | Санкт-Петербург</li>
						<li>+7 910 007 71 22 | Москва</li>
					</ul>
				</div>
				<div class="col-md-4">
					<h2>Оставить отзыв</h2>
					<span>Вы можете воспользоваться данной <a class="link" href="{{route('reviews')}}">формой</a>, чтобы оставить свой отзыв о проделанной работе</span>
				</div>
				<div class="col-md-4">
					<h2>Социальные сети</h2>
					<ul class="socials">
						<li><a href="https://vk.com/sevastopol.stage" class="link" target="_blank"><span class="icon-vkontakte">Вконтакте</span></a></li>
						<li><a href="https://www.instagram.com/daria_gargul" class="link" target="_blank"><span class="icon-instagram">Инстаграм</span></a></li>
						<li></li>
					</ul>
				</div>
			</div>
			<p class="copyright">© фотокнига.com - <?= date('Y')?></p>
			<span id="author">сделано <a href="https://github.com/arkataev" class="link" target="_blank">RedDev</a></span>
		</div>
	</footer>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
@yield('scripts')
<script type="text/javascript" src="{{ asset('/js/main.js')}} "></script>
</body>
</html>