@extends('layouts.layout')
@section('title')
	{{$project->header_1}} | {{$project->header_2}}
@endsection
@section('content')
	<div class="banner banner_xl" style="background-image: url('{{asset('/img/projects/'. $project->id . '/image_' . $project->mainImage .'.jpg')}}')">
		<div class="tint tint_circular"></div>
		<div class="container caption_text">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<h1>{{$project->header_1}}</h1>
					<p>{{$project->header_2}}</p>
				</div>
			</div>
		</div>
		<a class="scroll_down hidden-xs hidden-sm" href="#description">
			<div class="mouse_icon_block">
				<div class="mouse_scroll_icon"></div>
			</div>
		</a>
	</div>
	<article name="description" class="container">
		<div  class="projects">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
					@foreach($project->images as $image)
						@if(!$image->main)
							<a href="{{asset('/img/projects/'. $project->id . '/image_'. $image->id .'.jpg')}}" class="fancybox project_thumb project_thumb_md">
								<div class="image_wrapper">
									<img src="{{asset('/img/projects/'. $project->id . '/image_'. $image->id .'_cr.jpg')}}" alt="">
								</div>
								<div class="mask"></div>
								<div class="tint tint_linear"></div>
							</a>@endif
					@endforeach
				</div>
			</div>
		</div>
		<div class="block_info block_context">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-lg-12 last">
					<div class="block_info_item">
						<h2>Описание проекта</h2>
						<div class="service_description">
							{!!$project->description!!}
						</div>
						<button class="learn_more_btn order">Заказать</button>
					</div>
					<div id="vk_like"></div>
				</div>
			</div>
		</div>
	</article>
@endsection
@section('scripts')
	<script type="text/javascript" src="{{asset('/js/fancybox/source/jquery.fancybox.pack.js')}}"></script>
	<script type="text/javascript" src="//vk.com/js/api/openapi.js?123"></script>
@endsection