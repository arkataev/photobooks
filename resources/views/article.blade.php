@extends('layouts.layout')
@section('title')
	{{$article->title}}
@endsection
@section('content')
<div class="banner banner_md" style="background-image: url('{{asset('/img/articles/article_img_'. $article->id .'.jpg')}}')">
	<div class="tint tint_circular"></div>
	<div class="caption_text">
		<h1></h1>
	</div>
</div>
<article class="container">
	<div class="news_item block_context">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-lg-12">
				<div class="news_item_content">
					<div class="block_info_item block_info block_context">
						<h2>{{$article->title}}</h2>
						<p>{!! $article->description !!} </p>
						<div id="vk_like"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
@endsection
@section('scripts')
	<script type="text/javascript" src="//vk.com/js/api/openapi.js?123"></script>
@endsection