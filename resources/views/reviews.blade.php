@extends('layouts.layout')
@section('title')
	Отзывы
@endsection
@section('content')
	<div class="banner banner_sm" style="background-image: url('{{asset('/img/banners/reviews.jpg')}}')">
		<div class="tint tint_circular"></div>
		<div class="container caption_text">
			<div class="row">
				<div class="col-md-8 col-md-offset-4">
					<h1>Отзывы</h1>
				</div>
			</div>
		</div>
	</div>
	<article class="container">
		<ul class="reviews">
			@foreach($reviews as $review)
			<li class="review_item">
				<span class="block_info_label label_black label_right label_small">{{date('d/m/y', strtotime($review->date_add))}}</span>
				<div class="row">
					<div class="col-md-3">
						<div class="review_user_img">
							<img src="{{$review->user->vk_photo}}" alt="">
						</div>
					</div>
					<div class="col-md-9">
						<h3>{{$review->user->vk_full_name}}</h3>
						<p>{{$review->review_text}}</p>
					</div>
				</div>
			</li>
			@endforeach
		</ul>
		<nav class="projects_paginate">
			{{$reviews->links()}}
		</nav>

		@if(session('status'))
			<div class="alert alert-success review_item">
				{{session('status')}}
			</div>
		@endif

		<div id="review_form" class="review_item">
			<h2>Написать отзыв</h2>
		@if(!$user)
			<div class="authorize">
				<p>Чтобы оставить отзыв необходимо авторизироваться, с помощью Вконтакте</p>
				<a href="{{route('review_auth', ['id'=>'vk'])}}" id="vk_auth">
					<span class="icon-vkontakte">Войти</span>
				</a>
			</div>
		@else
			<img id="user_vk_photo" src="{{$user['vk_photo']}}" alt="">
			<span id="user_vk_fullname">{{$user['vk_full_name']}}</span>
			<form action="{{route('post_review')}}" method="post" id="post_review">
				<div class="form-group">
					<input type="hidden" name="id" value="{{$user['id']}}">
				</div>
				<div class="form-group">
					<label for="review_text"><h3>Ваш отзыв</h3></label>
					<textarea name="review_text" id="review_text" cols="130" rows="3" class="form-control" maxlength="255" required></textarea>
				</div>
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				<div class="form-group">
					<button type="submit" class="action_btn">Отправить</button>
				</div>
			</form>
		@endif
		</div>
	</article>
@endsection