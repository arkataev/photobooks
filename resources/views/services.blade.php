@extends('layouts.layout')
@section('title')
	Работа
@endsection
@section('content')
<div class="banner banner_sm" style="background-image: url('{{asset('/img/banners/services.jpg')}}'); background-position: center   ">
	<div class="tint tint_circular"></div>
	<div class="container caption_text">
		<div class="row">
			<div class="col-md-8 col-md-offset-4">
				<h1>Работа</h1>
			</div>
		</div>
	</div>
</div>
<article class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="block_info block_info_item">
				<div class="block_info_content">
					<h2>Фотокниги и Фотоальбомы</h2>
					<ul>
						<li>Школьные фотоальбомы и виньетки</li>
						<li>Семейные и персональные фотокниги</li>
					</ul>
				</div>
				<a href="{{route('service', ['id' => 1])}}" class="link">Подробнее</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="block_info block_info_item">
				<div class="block_info_content">
					<h2>Фотосессии</h2>
					<ul>
						<li>Персональная фотосъемка</li>
						<li>Групповая фотосъемка</li>
						<li>Съемка праздников и мероприятий</li>
					</ul>
				</div>
				<a href="{{route('service', ['id' => 2])}}" class="link">Подробнее</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="block_info block_info_item">
				<div class="block_info_content">
					<h2>Ретушь фотографий</h2>
					<ul>
						<li>Ретушь и обработка фотоматериалов</li>
					</ul>
				</div>
				<a href="" class="link">Подробнее</a>
			</div>
		</div>
	</div>
</article>

<div class="container-fluid ">
	<div class="row">
		<div class="gallery" id="sample_gallery">
			<ul>
				@foreach($images as $image)<a class="fancybox" href="{{asset('img/' . $image->route . '/image_' . $image->id . '.jpg')}}"><li>
						<div class="mask"></div>
						<div class="tint tint_linear"></div>
						<img src="{{asset('img/' . $image->route . '/image_' . $image->id . '_cr.jpg')}}" alt="">
					</li></a>@endforeach
			</ul>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	<script type="text/javascript" src="{{asset('/js/fancybox/source/jquery.fancybox.pack.js')}}"></script>
@endsection