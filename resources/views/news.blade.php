@extends('layouts.layout')
@section('title')
	Новости
@endsection
@section('content')
	<div class="banner banner_sm" style="background-image: url('{{asset('/img/banners/news.jpg')}}')">
		<div class="tint tint_circular"></div>
		<div class="container caption_text">
			<div class="row">
				<div class="col-md-8 col-md-offset-4">
					<h1>Новости</h1>
				</div>
			</div>
		</div>
	</div>
	<article class="container">
		<div class="news">
			@foreach($articles as $article)
			<div class="news_item block_info block_context">
				<div class="news_item_img">
					<p class="block_info_label label_black label_right label_small">{{date('d/m/y', strtotime($article->date_add))}}</p>
					<img src="{{asset('/img/articles/article_img_'. $article->id .'_rs.jpg')}}" alt="">
				</div>
				<div class="news_item_content ">
					<div class="block_info_item block_info block_context">
						<a href="{{route('article', ['id' => $article->id])}}"><h2>{{$article->title}}</h2></a>
						{!! str_limit($article->description, 200) !!}
						<a href="{{route('article', ['id' => $article->id])}}">Подробнее</a>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</article>
@endsection