@extends('layouts.layout')
@section('title')
	Портфолио
@endsection
@section('content')
<div class="banner banner_sm" style="background-image: url('{{asset('/img/banners/portfolio.jpg')}}')">
	<div class="tint tint_circular"></div>
		<div class="container caption_text">
			<div class="row">
				<div class="col-md-8 col-md-offset-4">
					<h1>Портфолио</h1>
				</div>
			</div>
		</div>
</div>
<article class="container">
	<div class="block_info">
		<div class="menu_nav block_info_item">
			@foreach($root as $index => $value)
				@if($value->id == $current)
					<a class="active" href="{{route('projects', ['category' => $value->id])}}"><h3>{{$value->name}}</h3></a>
				@else
					<a href="{{route('projects', ['category' => $value->id])}}"><h3>{{$value->name}}</h3></a>
				@endif
			@endforeach
		</div>
	</div>
	<div class="filter">
		<a class='active' href="#" id="*" >#все</a>
		@foreach($children as $child)
			<a href="#" id=".{{$child->slug}}">#{{$child->name}}</a>
		@endforeach
	</div>
	<div class="projects">
		@foreach($projects as $project)
			<a href="{{route('project', ['id'=> $project->id])}}" class="project_thumb project_thumb_md {{$project->type->slug}}">
				<div class="image_wrapper">
					<img src="{{asset('/img/projects/'. $project->id .'/image_'. $project->mainImage .'_cr.jpg')}}" alt="">
				</div>
				<div class="mask"></div>
				<div class="tint tint_linear"></div>
				<div class="image_caption">
					<h2>{{$project->header_1}} </h2>
					<span>#{{$project->category->name}}</span>
					<span>#{{$project->type->name}}</span>
				</div>
			</a>
		@endforeach
	</div>
	<nav class="projects_paginate">
		{{$projects->links()}}
	</nav>
</article>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('/js/isotope.pkgd.min.js') }}"></script>
	<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
@endsection