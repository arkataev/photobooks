@extends('layouts.layout')
@section('title')
	<?=mb_strtoupper(mb_substr($service->name, 0, 1)).mb_substr($service->name, 1, mb_strlen($service->name));?>
@endsection
@section('content')
<div class="banner banner_sm" style="background-image: url('{{asset('img/banners/services/'. $service->id .'/banner.png')}}')">
	<div class="tint tint_circular"></div>
	<div class="container caption_text">
		<div class="row">
			<div class="col-md-8 col-md-offset-3">
				<h1>{{$service->name}}</h1>
			</div>
		</div>
	</div>
</div>
<article class="container">
	<div class="block_info">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="block_info_item">
					<div class="block_stretch">
						{!!$service->description_short!!}
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 last">
				<div class="block_info_item">
					<div class="block_stretch">
						{!! $service->services_list !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-sm-12 col-xs-12">
			<div class="service_description">
				{!! $service->description !!}
			</div>
			<div class="block_featured_projects hidden-sm hidden-xs">
				<h3>Примеры:</h3>
				<div class="row">
					<div class="col-md-12">
						@foreach($examples as $example)<a href="{{route('project', ['id' => $example->id])}}"  class="project_thumb project_thumb_xs" id="{{$example->id}}">
							<div class="image_wrapper">
								<img src="{{asset('/img/projects/'. $example->id . '/image_'. $example->mainImage .'_cr.jpg')}}" alt="">
							</div>
							<div class="mask"></div>
							<div class="tint tint_linear"></div>
							<div class="image_caption">
								<h2>{{$example->header_1}}</h2>
								<span>#{{$example->category->name}}</span>
								<span>#{{$example->type->name}}</span>
							</div>
						</a>@endforeach
					</div>
				</div>
				<a href="{{route('projects')}}" class="link">Все работы</a>
			</div>
		</div>
		<div class="col-md-4 aside col-sm-12 col-xs-12">
			@if($service->id == 1)
			<div class="block_info calculator">
				<div class="block_info_item">
						<h2>Раcсчитать стоимость</h2>
						<form class="form-group data-input">
							<label for="book-type">Тип альбома</label>
							<select class="form-control" id="book-type"></select>
							<label for="book-size">Размер (см)</label>
							<select class="form-control" id="book-size"></select>
							<label for="foldsCountNumber">Развороты (шт)</label>
							<span id='foldsCountNumber'></span>
							<input class="range-slider" id="folds-count" type="range" step='1' >
							<label for="book-options">Обложка</label>
							<select class="form-control" id='book-options'></select>
							<div class="control-group">
								<label class="control control--checkbox" for="giftbox">Подарочная упаковка
									<input type="checkbox" name="giftbox" id="giftbox" value="600">
									<div class="control__indicator"></div>
								</label>
							</div>
							<label for="book-copies">Тираж (шт)</label>
							<span id='bookCopiesNumber'></span>
							<input id = 'book-copies' class="range-slider" type="range" min='1' max="100" step='1' value="1">
						</form>
						<div class="result">
							<span id="result"></span><span id ="currency"> руб.</span>
						</div>
				</div>
			</div>
			@endif
			<div class="block_info hidden-sm hidden-xs">
				<div class="block_info_item">
					<div class="block_stretch service_contacts">
						<h2>Есть вопрос?</h2>
						<p>Вы всегда можете получить интересующую информацию позвонив по указанным телефонам</p>
						<ul>
							<li>+7 978 071 35 43 | Крым</li>
							<li>+7 911 250 00 03 | Санкт-Петербург</li>
							<li>+7 910 007 71 22 | Москва</li>
						</ul>
					</div>
					<a href="{{route('contacts')}}" class="link">Контакты</a>
				</div>
			</div>
		</div>
	</div>
</article>
@endsection

@section('scripts')
	@if($service->id == 1)
		<script type="text/javascript" src="{{asset('/js/calc.js')}}"></script>

		<script>
			var bambook = {
			'name' : 'Персональный',										//	Название альбома
			'id'   : 'photobook',										    //	Id альбома
			'price': {                                                      //	Размер: Цена альбома
			'23x23'	:"",
			'29x19': "",
			'30x30'	:""
			},
			'folds' : {
			'min': 5,                                                   //	Минимальное кол-во разворотов
			'max': 40,                                                  //	Максимальное количество разворотов
			'fprice': 150                                               //	Цена разворота
			},
			'options' : [
			{
			'name': 'Стандартная',									//	Название опции
			'id': 'standard',										//	id опции
			'price': 0
			},
			{
			'name': 'Тканевая',									    //	Название опции
			'id': 'fiber',										    //	id опции
			'price': 650
			},
			{
			'name': 'Кожзам',									    //	Название опции
			'id': 'leather',										//	id опции
			'price': 650
			}
			]
			};


			var school = {
			'name': 'Школьный',						                            //	Название альбома
			'id': 'school',											            //	Id альбома
			'price': {                                                          //	Размер: Цена альбома
			'29x19': 350
			},
			'folds': {
			'min': 1,                                                   //	Минимальное кол-во разворотов
			'max': 40,                                                  //	Максимальное количество разворотов
			'fprice': 150                                               //	Цена разворота
			},
			'options': [
			{
			'name': 'Стандартная',									//	Название опции
			'id': 'standard',										//	id опции
			'price': 0
			},
			{
			'name': 'Тканевая',									    //	Название опции
			'id': 'fiber',										    //	id опции
			'price': 650
			},
			{
			'name': 'Кожзам',									    //	Название опции
			'id': 'leather',										//	id опции
			'price': 650
			}
			]
			};

			calculator([bambook, school]);
		</script>
	@endif
@endsection
