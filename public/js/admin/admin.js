/**
 * This file contains main AJAX requests and 
 * helper functions for Admin Controllers
 */

/* ----- -------- ----- */
/* ----- BANNERS ----- */
/* ----- -------- ---- */


$('.upload').change(changeUploadIcon);
$('.post').click(buttonLoading);
$('.delete').click(buttonLoading);

$('.banner_update').click(function () {
	var banner = {};
	var id  = $(this).data('banner_id');
	var file = $('#banner_' + id + ' input[type=file]')[0].files[0];
	banner.header_1 = $('#banner_' + id + ' input[name=header_1]').val();
	banner.header_2 =  $('#banner_' + id + ' input[name=header_2]').val();
	banner.target_url =  $('#banner_' + id + ' input[name=target_url]').val();
	banner.id =  id;
	file ? banner.image_upload = file : null;
	send('banners/update', createFormData(banner)).always(showModalResponse).done(function () {
		$('#alert-container').on('hide.bs.modal', function () {
			location.reload();
		})
	});
});


/* ----- -------- ----- */
/* ----- SERVICES ----- */
/* ----- -------- ----- */


// services update
$('.service_update').click(function () {
	var data = new FormData();
	var id = $(this).attr('id');
	data.append('id', id);
	// Get all instances of CKEditor
	for (instance in CKEDITOR.instances) {
		// Check if instance name string has current id
		if (instance.split('_').indexOf(id) > 0) {
			// remove number from instance name to get clear name
			// get data from CKEditor and push to data obj
			data.append(instance.substr(0, instance.length - 2 ), CKEDITOR.instances[instance].getData());
		}
	}
	
	send('services/update', data).always(function(data) {
		showModalResponse(data);
		buttonRestore('post', 'Сохранить');
	});
});


/* ----- СТАТЬИ----- */

// получем форму для редактирования статьи
$('.get_article').click(function (e) {
	e.preventDefault();
	send("articles/get", createFormData({'id' : $(this).data('article')})).done(function (data) {
		// добавляем данные из ответа в всплывающее окно проекта
		$('#article_modal .modal-content').append(data);
		// показываем всплывающее окно с формой редактирования
		$('#article_modal').modal('show');
	})
});

// при загрузке всплывающего окна
$('#article_modal').on('show.bs.modal', function () {
	// привыязываем событие на создание загрузку изображения для статьи
	$('#upload_thumb').change(articleImageUpload);
	// привязываем событие на сохранение проекта
	$('.post').click(postArticle);
	// привызываем событие на удаление проекта
	$('.delete').click(deleteArticle);
	$('.upload').change(changeUploadIcon);
	// иницилизируем CKEditor
	CKEDITOR.replace('description');
	$('.post').click(buttonLoading);
	$('.delete').click(buttonLoading);
});

// После закрытия всплывающего окна
$('#article_modal').on('hide.bs.modal', function () {
	// удаляем объект CKEDITOR
	CKEDITOR.instances.description ? CKEDITOR.instances.description.destroy(true) : null;
	// убираем прыдыдущую информацию
	$('#article_modal .modal-content article').detach();
	location.reload();
});

/**
 * Загружает изображение для статьи и обновляет плэйсхолдер
 *
 * @param upload
 */
function articleImageUpload(upload) {
	var file = $(upload.target)[0].files.item(0);
	var thumb = loadImageFromFile(file);
	$('#img_main_preview').attr('src', thumb.src);
}

/**
 * Добавляет новую статью или обновляет существующую
 */
function postArticle() {
	var article = {};
	var file = $('input[type=file]')[0].files[0];
	article.id = $(this).data('article_id');
	article.title = $('input[name=title]').val();
	article.description = CKEDITOR.instances['description'].getData();
	article.is_news = $('#is_news').prop('checked');
	file ? article.image_upload = file : null;
	//
	send($(this).data('target'), createFormData(article)).always(function(data) {
		showModalResponse(data);
		buttonRestore('post', 'Сохранить');
	});
}

/**
 *  Удаляет статью
 */
function deleteArticle() {
	send($(this).data('target'), createFormData({'id' : $(this).data('article_id')})).always(function(data) {
		showModalResponse(data);
		buttonRestore('delete', 'Удалить');
		$('#article_modal').modal('hide');
	});
}



/* ----- ГАЛЕРЕЯ ----- */

// add image
$('#admin_gallery .upload').change(function (upload) {
	var file = $(upload.target)[0].files.item(0);
	// create HTMLImageElement from file
	var image = loadImageFromFile(file, {"modal":"#img_modal", "target":"#image_crop"});
	// insert image into croppimg modal window
	$('body').find(image.dataset.target).attr('src', image.src);
	// create selection area on modal window
	var selection = imageCropperInit($(image.dataset.modal), $(image.dataset.target));
	// 
	$('#img_crop_set').click(function() {
		send(this.dataset.target, createFormData(getSelectedAreaData(image, selection), file)).done(function (e) {
			$(image.dataset.modal).modal('hide');
			e.type == 'success' ? location.reload() : alertDisplay(e,'.gallery');
		});
	});
});

// delete image
$('.img_delete').click(function () {
	var button =$(this);
	send('images/delete', createFormData( {'id':  button.attr('id')} )).done(function (e) {
		button.parent().detach();
	});
});



/* ----- ПРОЕКТЫ  ----- */

// получение данных формы для редактирования или создания проекта
$('.get_project').click(function (e) {
	e.preventDefault();
	var id  = $(this).data('project');
	// отправляем запрос на получение данных
	send('projects/get', createFormData({'id': id})).done(function (data) {
		// добавляем данные из ответа в всплывающее окно проекта
		$('#project_modal .modal-content').append(data);
		// показываем всплывающее окно с формой редактирования
		$('#project_modal').modal('show');
	});
});

// показываем всплывающее окно с формой
$('#project_modal').on('show.bs.modal', function () {
	var thumbsDel = {}, img_count = 0;
	// скрыть добавление основного изображения если миниатюра уже существует
	$('.main_thumb img').length == 1 ? $('.main_thumb .img_add').hide() : null;
	// скрыть добавление миниатюр если их количество == 6
	$('.thumbnails img').length == 6 ? $('.thumbnails .img_add').hide() : null;
	// создание основной миниатюры проекта
	$('#upload_thumb').change(createMainThumb);
	// создание дополнительных минатюр проекта
	$('#upload_thumbs').change(createThumbs);
	// получение подкатегорий для выбранной категории проекта
	$('#category').change(getProjectType);
	// привязываем событие на сохранение проекта
	$('#post_project').click(postProject);
	// привязываем событие на удаление проекта
	$('#delete_project').click(deleteProject);
	$('.upload').change(changeUploadIcon);
	// иницилизируем CKEditor после открытия всплывающего окна
	CKEDITOR.replace('description');
	// привязываем событие на удаление миниатюр из проекта
	$('.delete_thumb').click(function () {
		// показываем кнопку Добавить миниатюру если она была скрыта
		$(this).parent().parent().children('.img_add').show();
		// убираем миниатюру из набора
		$(this).parent().detach();
		// передаем в кнопку "Сохранить" id файла миниатюры для удаление с диска
		thumbsDel['delete_' + img_count++ ] = $(this).attr('id');
		$('#post_project').data("img_delete", thumbsDel);
	});
	// привязываем событие для анимации кнопок Сохранить и Удалить
	$('.post').click(buttonLoading);
	$('.delete').click(buttonLoading);
});

// После закрытия окна редактирования проекта
$('#project_modal').on('hide.bs.modal', function () {
	// удаляем объект CKEDITOR
	CKEDITOR.instances.description ? CKEDITOR.instances.description.destroy(true) : null;
	// убираем информацию предыдущего проекта
	$('#project_modal .modal-content article').detach();
	location.reload();
});


/**
 * Отсылает данные для создания нового проекта
 */
function postProject() {
	var project = {};
	// array of thumbails html objects
	var thumbs = Array.prototype.slice.call( $('#project_body .new_thumb'));
	project.thumbs = {};
	project.thumbs['data'] = {};
	project.thumbs['files'] = {};
	// get project id
	project.id = $(this).data('project_id') != undefined ? $(this).data('project_id') : null;
	// get header_1
	project.header_1 = $('#project_body input[name=header_1]').val();
	// get header_2
	project.header_2 = $('#project_body input[name=header_2]').val();
	// get description
	project.description = CKEDITOR.instances['description'].getData();
	// get sub_category
	project.category_id= $('#type').val();
	// get is_featured
	project.featured = $('#featured').prop('checked');
	// id изображений для удаления
	$(this).data('img_delete') != "" ? project.delete_img = $(this).data('img_delete') : undefined;
	// get thumbs
	var count = 0;
	thumbs.forEach(function (thumb) {
		project.thumbs['data'][thumb.file.name] = JSON.stringify(thumb.data.selection);
		project.thumbs['files']['image_' + count] = thumb.file;
		count++;
	});
	var flat = unpack(project);
	// unpack project object create FormData object and send it to controller
	send(this.dataset.target, createFormData(flat)).always(function(data) {
		showModalResponse(data);
		buttonRestore('post', 'Сохранить');
	}).done(function () {
		$('#alert-container').on('hide.bs.modal', function () {
			$('#project_modal').modal('hide');
		})
	});
};

/**
 * Посылает запрос на удаление выбранного проекта
 */
function deleteProject() {
	send($(this).data('target'), createFormData({'id' : $(this).data('project_id')})).always(function(data) {
		showModalResponse(data);
		buttonRestore('delete', 'Удалить');
		$('#project_modal').modal('hide');
	});
}

/**
 * Создает миниатюры из загруженных изображений
 * @param upload    массив загруженных файлов
 */
function createThumbs(upload) {
	var files = $(upload.target)[0].files;
	// Количество загруженных изображений
	var filesCount = files.length;
	// Общее количество существующих миниатюр в контейнере с миниатюрами
	var thumbsCount = $('.thumbnails .img_preview').length + $('.thumbnails .edited').length;
	// достаем загруженные файлы
	for (var i = 0; i < filesCount; i++) {
		// скрыть кнопку Добавить картинку когда количество миниатюр достигнет 6
		thumbsCount == 5 ? $('.thumbnails .img_add').hide() : false;
		// Не добавлять в контейнер более 6 миниатюр
		if (thumbsCount < 6) {
			// Создаем изображение из загруженного файла
			var thumb = loadImageFromFile(files.item(i));
			// Добавляем картинку с кнопкой "удалить" в контейнер с миниатюрами
			$('.thumbnails').prepend($('<li><button class="delete_thumb btn btn-danger">X</button></li>').append(thumb)).hide().fadeIn('slow');
			thumbsCount++;
		} else {
			continue;
		}
		// Привязываем событие для редактирование миниатюры по клику
		thumb.onclick = editThumb;
	}
	// удалить миниатюру при нажатии кнопки "Удалить" и показать кнопку "Добавить"
	$('.thumbnails .delete_thumb').click(function () {
		$(this).parent().detach();
		$('.thumbnails .img_add').show();
	});
}

/**
 * Создание миниатюры основного изображения из загруженного файла
 * @param upload       загруженный файл
 */
function createMainThumb(upload) {
	var file = $(upload.target)[0].files.item(0);
	var thumb = loadImageFromFile(file);
	thumb.dataset.main = true;
	var thumbContainer = $('.main_thumb');
	// restrict main thumbs number to 1
	if (thumbContainer.children().length == 1) {
		$('#img_main_preview').attr('src', thumb.src);
		// append upladed image in main_thumb list
		thumbContainer.prepend($('<li><button class="delete_thumb btn btn-danger">X</button></li>').append(thumb)).hide().fadeIn('slow');
		// delete thumb on Delete button click
		$('.main_thumb .delete_thumb').click(function () {
			$(this).parent().detach();
			$('.main_thumb .img_add').show();
		});
		thumb.onclick = editThumb;
	}
	$('.main_thumb .img_add').hide();
}

/**
 * Обрезка изображений и обновление миниатюр
 *
 * При клики на миниатюру отображает всплывающее окно для с полем обрезки
 * После обрезки изменяет изображение исходной миниатюры и привязывает к нему
 * данные из области обрезки (длина, ширина, позиция обрезки) для отправки в контроллер
 *
 * @param thumb
 */
function editThumb(thumb) {
	var currentThumb = thumb.target;
	// создаем изображение из загруженного файла
	var image = loadImageFromFile(currentThumb.file, {"modal":"#img_modal", "target":"#image_crop"});
	// отобразить миниатюру в окне для обрезки изображения
	$('body').find(image.dataset.target).attr('src', image.src);
	// создаем область обрезки
	var selection = imageCropperInit($(image.dataset.modal), $(image.dataset.target));
	// Если нажата кнопка Сохранить:
	document.getElementById('img_crop_set').onclick = function () {
		// Изменить изображение в миниатюре на область обрезки
		updateThumb(image, selection, currentThumb);
		// Проверить является ли изменное изображение Главной картинкой
		image.main = $(currentThumb).parent().parent().hasClass('main_thumb') ? true : false ;
		image.file.main = $(currentThumb).parent().parent().hasClass('main_thumb') ? true : false ;
		// собрать данные обласи обрезки (длина, ширина, позиция) и прикрепить к элементу миниатюры
		currentThumb.data = {
			'selection' : getSelectedAreaData(image, selection)
		};
		// скрыть модальное окно
		$(image.dataset.modal).modal('hide');
		// удалить области обрезки
		selection.cancelSelection();
	};
}

/**
 * Получение подкатегорий для выбранной категории проекта
 *
 * При выборе категории проект отправляет запрос на получение
 * подкатегорий и отображает их в дополнительном списке после получения
 * результата
 *
 * @param e
 */
function getProjectType(e) {
	$('#project_type').fadeOut('fast');
	$('#type option').detach();
	send('projects/get_category', createFormData({'id' : e.target.selectedOptions[0].value})).done(function (response) {
		response.type.forEach(function (item) {
			$('#type').append($('<option value="' + item.id + '">' + item.name + '</option>'));
		})
		response.type.length ? $('#project_type').fadeIn('slow') : null;
	})
}


/* ----- ОТЗЫВЫ  ----- */

$('.reviews #accept').click(function () {
	send($(this).data('target'), createFormData({'id' : $(this).data('review_id')})).done(function () {
		location.reload();
	});
});

$('.reviews #deny').click(function () {
	send($(this).data('target'), createFormData({'id' : $(this).data('review_id')})).done(function () {
		location.reload();
	});
});


/* ----- ДОПОЛНИТЕЛЬНО ----- */

/**
 * Sends AJAX requests
 *
 * @param url
 * @param data
 * @returns {*}
 */
function send(url, data) {
	return $.ajax({
		'url' : url,
		'headers': {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') // Laravel security token
		},
		'method' : 'post',
		'data' : data,
		'cache' : false,
		'processData' : false,
		'contentType': false
	});
}

/**
 * Shows modal windows with cropping overlay
 *
 * @param cropWindow
 * @param cropObjImage
 * @docs http://odyniec.net/projects/imgareaselect/usage.html
 * @returns {*}
 */
function imageCropperInit(cropWindow, cropObjImage) {
	if (typeof cropObjImage.imgAreaSelect == 'function') {
		// create ImgAreaSelected plugin instance
		var selection = cropObjImage.imgAreaSelect({
			aspectRatio: "1:1",
			handles: true,
			instance: true,
			show: true,
			parent: $('#img_modal .modal-content'),
			persistent: true,
			resizable:true,
			x1: 0,
			y1: 0,
			x2: 270,
			y2: 270
		});
		// show imgArea overlay with selected canvas on modal window show
		cropWindow.modal('show').on('shown.bs.modal', function () {
			selection.update();
		});

		return selection;
	}else {
		return false;
	}
}


/**
 * Returns data from crop area of crop-modal-window
 *
 * @param img
 * @param selection
 * @returns {{x1: (number|*), y1: (*|number), height: *, width: *, wr: number, hr: number}}
 */
function getSelectedAreaData(img, selection) {
	var s_data = selection.getSelection();
	var crop_canvas = $('#image_crop');

	return {
		"x1": s_data.x1,
		"y1": s_data.y1,
		"height": s_data.height,
		"width" : s_data.width,
		"wr": img.dataset.width  / crop_canvas.width(),     // width ratio
		"hr" : img.dataset.height / crop_canvas.height(),    // height ratio
		"main" : img.main
	}
}


/**
 * Loads image from uploaded file into HTMLImageElement
 *
 * @param file  object  Uploaded file object
 * @param data  object  {"class" : "thumbnails class",
 *                       "modal" : "id of div where thumbnails preview will be displayed,
 *                       "target" : img in modal div where file should be inserted}
 * @returns {HTMLImageElement}
 */
function loadImageFromFile(file, data={}) {
	var img = new Image();
	// set id of modal window to load image into
	data.modal  !== undefined ? img.dataset.modal = data.modal  : null;
	// set id of target object to load image data into
	data.target !== undefined ? img.dataset.target = data.target : null;
	// set new thumbnail class
	img.classList.add("img_preview");
	// attach file to thumbnail object
	img.file = file;
	// create new object URL
	img.src = window.URL.createObjectURL(file);
	// when image is loaded release object URL and display image
	img.onload = function () {
		this.dataset.width = this.naturalWidth;
		this.dataset.height = this.naturalHeight;
		window.URL.revokeObjectURL(this.src);
	};

	return img;
}

/**
 * Updates image thumbnail
 *
 * @param image         HTMLImageElement
 * @param selection     ImgAreaSelect object
 * @param thumb         HTMLImageElement thumb
 */
function updateThumb(image, selection, thumb) {
	var imgDivWidth = 127;      // thumbnail div width
	var imgDivHeight = 127;     // thumbnail div height
	var scaleX = imgDivWidth / (selection.getSelection().width || 1);
	var scaleY = imgDivHeight / (selection.getSelection().height || 1);
	$(thumb).css({
		width: Math.round(scaleX * $(image.dataset.target).width()) + 'px',
		height: Math.round(scaleY * $(image.dataset.target).height()) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.getSelection().x1) + 'px',
		marginTop: '-' + Math.round(scaleY * selection.getSelection().y1) + 'px'
	});
	$(thumb).addClass('new_thumb edited');
	$(thumb).removeClass('img_preview');
}


/**
 * Creates new Form data object from flat objects key - value pairs
 *
 * @param img       File object to append to form
 * @param data      Data object
 * @returns {*}
 */
function createFormData(data, img) {
	var form = new FormData();
	for (var key in data) {
		form.append(key, data[key]);
	}
	img ? form.append('image_upload', img) : null;

	return form;
}


/**
 * Flattens multidimensional object
 *
 * @param object
 * @returns {{}}
 */
function unpack(object) {
	var flat = {};
	for (var key in object) {
		if (typeof object[key] == 'object' && object[key] instanceof File != true) {
			var branch = unpack(object[key]);
			for (var leaf in branch) {
				flat[leaf] = branch[leaf];
			}
		}else if (typeof object[key] == 'array') {
			flat.push(object[key]);
		}else {
			flat[key] = object[key];
		}
	}

	return flat;
}


function deleteConfirmModal() {
	// TODO::
}



function buttonLoading() {
	$(this).children('.loader-inner').html('' +
		'<div></div>' +
		'<div></div>' +
		'<div></div>' +
		'');
	$(this).prop('disabled', true)
}

function buttonRestore(btn_class, name) {
	let $button  = $('.' + btn_class);
	$button.children('.loader-inner').html(name);
	$button.prop('disabled', false);
}


function changeUploadIcon() {
	$(this).parent().is('.icon-ok-circled2') ? undefined : $(this).parent().toggleClass('icon-plus');
	$(this).parent().is('.icon-ok-circled2') ? undefined : $(this).parent().toggleClass('icon-ok-circled2');
}





/**
 * Отображение всплывающего окна с ответом от сервера
 * @param data
 */
function showModalResponse(data) {
	$('#alert-container .message li').detach();
	var messages = [];
	var messageClass = data.status && data.status == 422 ? 'icon-cancel-circled2' : 'icon-ok-circled2';
	var response = data.responseJSON || data;
	for (var key in response) {
		if (typeof response[key] == 'Array') {
			response[key].foreach(function (item) {
				messages.push(item);
			})
		}else {
			messages.push(response[key]);
		}
	}
	$('#message_type').attr('class', messageClass);
	messages.forEach(function (item) {
		$('#alert-container .message').append($('<li>' + item + '</li>'));
	})
	$('#alert-container').modal('show');
}


// Фикс для открытия нескольких модальных окон одновременно на одной странице
// @author http://stackoverflow.com/questions/19305821/multiple-modals-overlay
$(document).on('show.bs.modal', '.modal', function () {
	var zIndex = 1040 + (10 * $('.modal:visible').length);
	$(this).css('z-index', zIndex);
	setTimeout(function() {
		$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	}, 0);
});
// Фикс для корректной работы скролла при открытии нескольких модальных окон на одной странице
$(document).on('hidden.bs.modal', '.modal', function () {
	$('.modal:visible').length && $(document.body).addClass('modal-open');
});

