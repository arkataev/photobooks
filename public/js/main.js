/**
 * Created by arkat on 07.06.2016.
 */

var $filter = null;    // projects filter

$(document).ready(function() {

	$('#navbar_show').click(showMenu);

	function showMenu() {
		$('#navbar_collapse').animate({
			display: 'block',
			width: 'toggle',
		}, 300);
	}

	// Инициализация ВК
	if (typeof VK !== 'undefined') {
		VK.init({
			apiId: 5207736,
			onlyWidgets: true
		});

		// Вк кнопка "Мне нравится"
		VK.Widgets.Like('vk_like');
	}

	// Projects Isotope filter
	if(typeof Isotope !== 'undefined') {
		var $filter  = $('.projects').imagesLoaded(function() {
			$filter.isotope({
				itemSelector: '.project_thumb',
				layoutMode: 'masonry',
				percentPosition: true,
			})
		});

		// Projects filter
		$('.filter').on( 'click', 'a', function(e) {
			e.preventDefault();
			var filterValue = $(this).attr('id');
			$filter.isotope({ filter: filterValue });
		});

	}

	if(typeof $.flexslider !== 'undefined') {
		// FlexSlider main banner
		$('#main_banner').flexslider({
			animation: "fade",
			slideshowSpeed: 8000,
			pauseOnHover: true,
			touch: false,
			controlNav: false,
			useCSS: true,
		});
		// FlexSlider reviews
		$('#reviews').flexslider({
			animation: "slide",
			animationLoop: true,
			itemWidth: 400,
			itemMargin: 5,
			minItems: 1,
			maxItems: 4,
			controlNav: false,
		});
	}
	// FancyBox
	if(typeof $.fancybox !== 'undefined') {
		$(".fancybox").attr('rel','gallery').fancybox({
			padding : 0,
			helpers: {
				overlay: {
					locked: false
				}
			}
		})
	}
	// всплывающее меню в главном меню
	$('#dd-toggle').hover(function () {
		$('#dd-menu').slideToggle('fast');
	})

	$('.order').click(function () {
		location.href = '/contacts';
	})


});

$('a').on('click',function (e) {
	$(this).parent().children('a').removeClass('active');
	$(this).addClass('active');
})

// Navbar onscroll animation
$(document).on("scroll", function() {
	if($(document).scrollTop() > 200) {
		$(".navbar_bg").width("100%");
	} else {
		$(".navbar_bg").width("10%");
	}
});





// Плавный скролл по клику на главной странице проекта
// @author https://css-tricks.com/snippets/jquery/smooth-scrolling/
$('.scroll_down').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		if (target.length) {
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 1000);
			return false;
		}
	}
});

/* Yandex.Metrika counter*/
try {
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function () {
			try {
				w.yaCounter34311065 = new Ya.Metrika({
					id: 34311065,
					clickmap: true,
					trackLinks: true,
					accurateTrackBounce: true,
					webvisor: true
				});
			} catch (e) {
			}
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () {
				n.parentNode.insertBefore(s, n);
			};
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else {
			f();
		}
	})(document, window, "yandex_metrika_callbacks");
}catch (e) {
	console.log('Yandex Metrica is not connected' + ' ('+ e.message + ')');
}