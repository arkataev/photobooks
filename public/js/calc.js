/**
 *  Калькулятор стоимости альбомов 
 *  @param array books  массив объектов с информацией об альбомах
 */

function calculator(books) {

	var shelf = [];

	/* 	Range - function
	 *	@param int a, b (a - минимальное значение, b - максимальное)
	 *	return array result
	 *	Возвращает массив последовательных числе в заданном диапазоне от a до b
	 */
	var range = function (a, b) {
		var result = [];
		var i = a;
		for (i; i <= b; i++) {
			result.push(i);
		}
		return result;
	};

	var getElement = function (elementId) {
		return document.getElementById(elementId);
	};


	books.forEach(function (item) {
		shelf.push(new Book(item));
	});

	/*	Инициализация объекта-библиотеки */
	var library = new Library(shelf);

	/* 	Class Book
	 *	@param objects array sizes - размеры и цены
	 *	@param string name - название книги
	 *	@param string id - id книги
	 *	@param int maxFolds - максимальное количество разворотов альбома
	 *	@param int foldPrice - цена за один разворот
	 *	@param objects array options - список дополнительных опций с ценами (обложка, упаковка и пр.)
	 */
	function Book(book) {
		this.name = book.name;
		this.id = book.id;
		this.sizes = book.price;
		this.folds = range(book.folds.min, book.folds.max);
		this.options = book.options;
		this.foldPrice = book.folds.fprice;
	}

	Book.prototype.hasOptions = function () {
		return  this.options ? true : false;
	};
	
	/*
	 Library
	 */
	function Library(books) {
		this.books = books;
		this.currentBook = this.books[0];
	}

	Library.prototype.currentBookName = function () {
		return this.currentBook.name;
	};

	Library.prototype.currentBookSizes = function () {
		return this.currentBook.sizes;
	};

	Library.prototype.currentBookOptions = function () {
		return this.currentBook.options;
	};

	Library.prototype.selectBook = function () {
		var selectedBook;
		selectedBook = getElement('book-type').selectedIndex;
		this.currentBook = this.books[selectedBook];
		/*	Обновляем данные для выбранной книги	*/
		libraryUI.displayBookSizes();
		libraryUI.displayBookOptions();
		libraryUI.displayBookFolds();
		libraryUI.displayBookPrice();
	};

	Library.prototype.getCurrentBookPrice = function () {
		var bookPrice = 0;
		var selectedOptions, selectedSize, foldsNumber, numberOfCopies, giftboxPrice = 0;
		selectedOptions = libraryUI.selectedOptions();
		selectedSize = libraryUI.selectedSize();
		foldsNumber = libraryUI.getRangeValue('folds-count', 'foldsCountNumber');
		numberOfCopies = libraryUI.getRangeValue('book-copies', 'bookCopiesNumber');
		if (document.getElementById('giftbox').checked === true) {
			giftboxPrice = Number(document.getElementById('giftbox').value);
		}
		bookPrice += this.currentBookSizes()[selectedSize];
		// Если размер выбран размер 30х30 -> установить foldPrice = 200
		if (selectedSize === '30x30') {
			this.currentBook.foldPrice = 200
		}
		// Если размер выбран размер 23х23 -> установить foldPrice = 160
		if (selectedSize === '23x23') {
			this.currentBook.foldPrice = 160
		}
		// Если размер выбран размер 29х19  -> установить foldPrice = 170
		if (selectedSize === '29x19' && this.currentBook.id == 'photobook') {
			this.currentBook.foldPrice = 170
		}
		bookPrice += this.currentBook.foldPrice * foldsNumber;
		bookPrice = bookPrice * numberOfCopies;
		bookPrice += giftboxPrice;
		// ? есть выбранные опции -> добавить их цену к стоимости
		if (selectedOptions.length > 0) {
			bookPrice += Number(selectedOptions)
		}
		return bookPrice;
	};

	var libraryUI = {
		/* 	Метод displayBookName
		 *	Создает DOM - элементы и 
		 *	Выводит на экран список названий книг добавленных в библиотеку
		 *	@param void
		 *	return void	*/
		displayBookName: function () {
			var _this = this;
			var option, bookTypeHTML;
			bookTypeHTML = getElement('book-type');
			library.books.forEach(function (book) {
				option = _this.createElement('option');
				option.value = book.id;
				option.textContent = book.name;
				bookTypeHTML.appendChild(option);
			});
		},
		/* 	Метод displayBookSizes
		 *	Создает DOM - элементы и 
		 *	Выводит на экран список размеров книг добавленных в библиотеку
		 *	@param void
		 *	return void	*/
		displayBookSizes: function () {
			getElement('book-size').innerHTML = '';
			var option, bookSizeHTML;
			bookSizeHTML = getElement('book-size');
			for (var size in library.currentBook.sizes) {
				option = libraryUI.createElement('option');
				option.value = size;
				option.textContent = size;
				bookSizeHTML.appendChild(option);
			}
		},
		/*	Метод displayBookFolds
		 *	Редактирует DOM - элемент #folds-count устанавливает максимальное и 
		 *	минимальное значение для слайдера выбора количества разворотов книги
		 *	@param void
		 *	return void
		 */
		displayBookFolds: function () {
			var range, bookFoldsParams;
			bookFoldsParams = library.currentBook.folds;
			range = getElement('folds-count');
			if (this.selectedSize() === '30x30') {
				range.min = 8;
			}else if (this.selectedSize() === '23x23') {
				range.min = 3;
			}else {
				range.min = Math.min.apply(null, bookFoldsParams);
			}			                                                        //	Возвращает минимальное значение в массиве bookFoldsParams
			range.max = Math.max.apply(null, bookFoldsParams);					//	Возвращает максимальное значение в массиве bookFoldsParams
			range.value = range.min;
		},

		/* 	Метод displayBookOptions
		 *	Создает DOM - элементы и 
		 *	Выводит на экран список дополнительных опций книг добавленных в библиотеку
		 *	@param void
		 *	return void	*/
		displayBookOptions: function () {
			getElement('book-options').innerHTML = '';
			var _this = this;
			var item, itemInput, itemLabel, bookOptionsHTML;
			bookOptionsHTML = getElement('book-options');
			if (library.currentBook.hasOptions()) {
				library.currentBook.options.forEach(function (option) {
					// Создаем HTML - элементы
					itemInput = _this.createElement('option');
					// Присваиваем значения
					itemInput.id = option.id;
					//  Если размер книги  === 30х30 -> стоимость обложки == 750
					if (_this.selectedSize() === '30x30' && itemInput.id != 'standard') {
						itemInput.value = 750
					}else {
						itemInput.value = option.price;
					}
					itemInput.textContent = option.name;
					//	Добавляем элементы в DOM
					bookOptionsHTML.appendChild(itemInput);
				})
			}
		},

		/* 	Метод displayBookPrice
		 *	Создает DOM - элементы и 
		 *	Выводит на экран общую стоимость выбранной книги
		 *	@param void
		 *	return void	*/
		displayBookPrice: function () {
			var priceHTML;
			priceHTML = getElement('result');
			priceHTML.textContent = library.getCurrentBookPrice();
		},

		/* 	Метод selectedSize
		 *	Возвращает выбранный размер книги 
		 *	@param void
		 *	return string	sizesHTML[sizesHTML.selectedIndex].value */
		selectedSize: function () {
			var sizesHTML;
			sizesHTML = getElement('book-size');
			return sizesHTML[sizesHTML.selectedIndex].value;
		},

		/*	Метод getRangeValue
		 *	Возвращает текущее значение слайдера <input type='range'> и помещает его в выбранный DOM-элемент
		 *	@param string rangeElement - #id элемента range - слайдера
		 *	@param string outputElement - #id элемента, куда нужно поместить текущее значение слайдера
		 *	return int Number(getElement(rangeElement).value	*/
		getRangeValue: function (rangeElement, outputElement) {
			getElement(outputElement).innerHTML = getElement(rangeElement).value;
			return Number(getElement(rangeElement).value);
		},

		/*	Метод selectedOptions
		 *	Возвращает выбранные дополнительные опции в виде массива
		 *	@params void
		 *	return array selectedOptions - содержит объекты <input type = 'checkbox'> - выбранные пользователем	*/
		selectedOptions: function () {
			var options = getElement('book-options').options;
			return options[options.selectedIndex].value
		},

		bindEventListener: function () {
			var _this = this;
			var book_size = getElement('book-size');
			//  Обновляет диапазон разваротов книги в зависимости от выбранного размера
			book_size.addEventListener('change', function () {
				_this.displayBookFolds()
			}, false);
			book_size.addEventListener('change', function () {
				_this.displayBookPrice()
			}, false);
			book_size.addEventListener('change', function () {
				_this.displayBookOptions()
			}, false);
			getElement('folds-count').oninput = function () {
				_this.displayBookPrice()
			};
			getElement('book-copies').oninput = function () {
				_this.displayBookPrice()
			};
			getElement('book-options').onchange = function () {
				_this.displayBookPrice()
			};
			getElement('giftbox').onchange = function () {
				_this.displayBookPrice()
			};
			getElement('book-type').onchange = function () {
				library.selectBook()
			};
		},

		createElement: function (element) {
			return document.createElement(element);
		}
	};
	/*	Вывод на экран HTML - элементов */
	libraryUI.displayBookName();							    //	Названия книг
	libraryUI.displayBookSizes();								//	Размеры
	libraryUI.displayBookOptions();								//	Дополнительные опции
	libraryUI.displayBookFolds();								//	Развороты
	libraryUI.displayBookPrice();								//	Стоимость книги

	/*	Добавление обработчика событий */
	libraryUI.bindEventListener();
}