# Фотокнига.com
Сайт галерея-фотоблог с возможностью размещения фотоматериала в виде проектов или новостей с текстовым описанием, написания пользовательских отзывов.
Панель управления позволяет редактировать текстовые и фото - материалы, удобно загружать и создавать миниатюры для нескольких изображений, модерировать отзывы пользователей.

## Технологии
* PHP (Laravel 5.2)
* JavaScript (JQuery)
* AJAX
* CSS (SASS)

## Особенности
- Пакетная обработка и создание миниатюр загруженных изображений
- Асинхронное обновление информации в панели управления
- Авторизация пользователей через социальную сеть Вконтакте


