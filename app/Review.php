<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

	/**
	 * Elequent special method to get related tables
	 * This means that 'reviews' table has many relations to 'users' table (multiple reviews has one user)
	 * @docs (https://laravel.com/docs/5.2/eloquent-relationships#one-to-many)
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}
}
