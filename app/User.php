<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	protected $fillable = ['vk_id', 'vk_full_name', 'vk_photo'];
}
