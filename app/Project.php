<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Project extends Model
{

	/**
	 * Special Model attribute that helps to append properties to model on loading
	 * To use it, appropriate 'getters' should be implemented
	 * @docs (https://laravel.com/docs/5.1/eloquent-serialization#appending-values-to-json)
	 *
	 * @var array
	 */
	protected $appends = ['type', 'category', 'mainImage'];


	public function getMainImageAttribute()
	{
		$main_image = DB::table('images')->where([
			['project_id', $this->id],
			['main', 1]
		])->first();

		return $this->attributes['mainImage'] = $main_image ? $main_image->id : null;
	}
	
	/**
	 * @return Categorie        Root Categories where current Project belongs
	 */
	public function getCategoryAttribute()
	{
		return DB::table('categories')->where('id', $this->type->parent_id)->first();
	}

	/**
	 * @return Categorie        Children Categories where current Project belongs
	 */
	public function getTypeAttribute()
	{
		return  DB::table('categories')->where('id', $this->category_id)->first();
	}
	
	/**
	 * Elequent special method to get related tables
	 * This means that 'projects' table has many relations to 'categories' table (one project has many categories)
	 * @docs (https://laravel.com/docs/5.2/eloquent-relationships#one-to-many)
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function categories()
	{
		return $this->belongsTo('App\Categorie', 'category_id', 'id');
	}


	public function images()
	{
		return $this->hasMany('App\Image', 'project_id', 'id');
	}
}
