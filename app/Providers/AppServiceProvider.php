<?php

namespace App\Providers;

use App\Project;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('unique_featured', function ($attributes, $value, $parameters, $validator){
        	// если пришел запрос со свойством featured == 'true'
        	    // проверяем есть ли другой проект со свойством featured == 1
            return $value == 'true' ? !Project::where([['featured', 1], ['id', '<>', $validator->getData()['id']] ])->first() : true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
