<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
//        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        switch ($e) {
            case $e instanceof ModelNotFoundException:
                return $this->renderModelNotFoundException($e);
                break;
            case $e instanceof FileException:
                return response()->json(['message' => $e->getMessage()], 422);
                break;
            case $e instanceof FatalErrorException || $e instanceof \ErrorException:
                Log::error('Ошибка : ' . $e->getMessage() . ' В строке: ' . $e->getLine() . 'В файле:' . $e->getFile());
                return response()->json(['message' => 'Ой! Что-то пошло не так ;( Ошибка: ' . $e->getMessage()], 422);
                break;
            default:
                return parent::render($request, $e);
        }
    }

    public function renderModelNotFoundException(ModelNotFoundException $e)
    {
        echo 'this model not found';
    }




}
