<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->file('image_upload')->isValid()) {
            throw new FileException('Файл превышает лимит 2МБ');
        }else {
            // custom error messages 
            $messages = [
                'image_upload.dimensions' => 'Неверный размер изображения',
                'image_upload.mimes' => 'Неверный формат изображения',
                'image_upload.image' => 'Необходимо загрузить изображение',
            ];
            // if file is from galleryController, use different dimensions rules
            if ($request->is('admin/images/*')) {
                $rules['image_upload'] = 'image|mimes:jpeg,png,jpg';
            }else {
                $rules['image_upload'] = 'image|dimensions:min_width=1900,min-height=700|mimes:jpeg,png,jpg';
            }
           
            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->all(), 'type' => 'error']);
            }
        }

        return $next($request);
    }
}
