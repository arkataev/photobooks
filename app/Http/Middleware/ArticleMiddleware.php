<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;
use App\Article;

class ArticleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rules = [
            'title' => 'required|max:50',
            'description' => 'required'
        ];
        
        $validator = Validator::make($request->all(), $rules, Article::$messages);
        
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->all(), 'type' => 'error']);
        }
        
        return $next($request);
    }
}
