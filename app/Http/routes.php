<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Article;
use App\Image;
use App\Project;


/**
 * Front Controllers
 */
// index page
Route::get('/', 'HomeController@index')->name('home');
// all projects (portfolio)
Route::get('projects/{category?}', 'ProjectsController@index')->name('projects');
// Страница проекта
Route::get('project/{id}', function ($id) {
	return view('project', ['project' => Project::where('id', $id)->first()]);
})->name('project');
// all services
Route::get('services', function () {
	return view('services', ['images' => Image::where('project_id', 0)->orderByRaw("RAND()")->take(16)->get()]);
})->name('services');
// service
Route::get('services/{id}', 'ServicesController@getService')->name('service');
// Список статей (новости)
Route::get('articles', function () {
	return view('news', ['articles' => Article::where('news', 1)->orderBy('date_add', 'desc')->get()]);
})->name('articles');
// Статья по Id
Route::get('articles/{id}', function ($id) {
	return view('article', ['article' => Article::where('id', $id)->first()]);
})->name('article');
// reviews
Route::get('reviews', 'ReviewsController@index')->name('reviews');
// post_review
Route::post('reviews/post', 'ReviewsController@postReview')->name('post_review');
//review_auth
Route::get('user/auth/{id}', 'ReviewsController@authenticate')->name('review_auth');
// контакты
Route::get('contacts', function () { return view('contacts'); })->name('contacts');


/**
 *  Контролллеры авторизации
 */
// Авторизация через ВКонтакте
Route::get('auth/vk', 'Auth\VkAuthController@authenticate')->name('vk_auth');
// Авторизация администратора через форму логин-пароль
Route::post('admin/auth', 'Admin\AdminLoginController@login');
Route::get('admin/logout', 'Admin\AdminLoginController@logout')->name('logout');
Route::get('admin/login', function (){ return view('admin.login'); });

/**
 *  Контролллеры Админки
 */
Route::group(['namespace' => 'Admin','prefix' => 'admin','middleware' => 'auth'], function (){
	// banners
	Route::group(['prefix' => 'banners'], function (){
		Route::get('/', 'BannersController@index')->name('admin_banners');
		Route::post('update', 'BannersController@updateBanner')->name('banner_update');
	});

	// services
	Route::group(['prefix' => 'services'], function (){
		Route::get('/', 'ServicesController@index')->name('admin_services');
		Route::post('update', 'ServicesController@updateService')->name('service_update');
	});
	
	// articles
	Route::group(['prefix' => 'articles'], function () {
		Route::get('/', 'ArticlesController@index')->name('admin_articles');
		Route::post('delete', 'ArticlesController@deleteArticle')->name('article_delete');
		Route::post('get', 'ArticlesController@getArticle')->name('article_get');
		Route::post('post', 'ArticlesController@postArticle')->name('article_post');
		Route::post('update', 'ArticlesController@updateArticle')->name('article_update');
	});

	// projects
	Route::group(['prefix' => 'projects'], function (){
		Route::get('/', 'ProjectsController@index')->name('admin_projects');
		Route::post('delete', 'ProjectsController@deleteProject')->name('project_delete');
		Route::post('get', 'ProjectsController@getProject')->name('project_get');
		Route::post('get_category', 'ProjectsController@getProjectCategory')->name('project_get_category');
		Route::post('post', 'ProjectsController@postProject')->name('project_post');
		Route::post('update', 'ProjectsController@updateProject')->name('project_update');
	});
	
	// images
	Route::group(['prefix' => 'images'], function (){
		Route::get('/', 'ImagesController@index')->name('admin_gallery');
		Route::post('delete', 'ImagesController@deleteImage')->name('image_delete');
		Route::post('post', 'ImagesController@postImage')->middleware('image')->name('image_post');
	});

	// reviews
	Route::group(['prefix' => 'reviews'], function (){
		Route::get('/', 'ReviewsController@index')->name('admin_reviews');
		Route::post('approve', 'ReviewsController@approveReview')->name('review_approve');
		Route::post('deny', 'ReviewsController@denyReview')->name('review_deny');
	});
});