<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

abstract class AuthController extends Controller
{

	public static function getInstance($auth_id = null)
	{
		switch ($auth_id) {
			case 'vk' :
				return new VkAuthController();
			// case 'fb' :
			// Facebook authorization controller
			default :
				// Стандартная форма логин/пароль
				return new FormAuthController();
		}
	}
}
