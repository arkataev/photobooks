<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FormAuthController extends AuthController
{

	public function login(Request $request)
	{
		return  Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password'), 'is_admin' => 1]);
	}

	public function logout()
	{
		Auth::logout();

		return redirect('admin/login');
	}
}
