<?php
/**
 * @author reddev
 * @web https://bitbucket.org/arkataev
 * @date: 06.08.2016
 */

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\User;


class VkAuthController extends SocialAuthController
{
	private $client_id = 5207736;
	private $client_secret = '93n6ZGwemThcEAfiusLK';
	private $request;
	private $vk_user;


	public function authorizeAPI()
	{
		$auth_query = http_build_query([
			'client_id' => $this->client_id,
			'redirect_uri' => route('vk_auth'),
			'display' => 'popup',
			'response_type' => 'code',
			'revoke' => 1
		]);

		return redirect("https://oauth.vk.com/authorize?{$auth_query}");
	}

	protected function getToken()
	{
		$auth_query = http_build_query([
			'client_id' => $this->client_id,
			'redirect_uri' => route('vk_auth'),
			'client_secret' => $this->client_secret,
			'code' => $this->request->input('code')
		]);
		$auth = json_decode(file_get_contents("https://oauth.vk.com/access_token?{$auth_query}"));

		return isset($auth->user_id) ? ['token' => $auth->access_token, 'user_id' => $auth->user_id] : false;
	}

	protected function getUserData()
	{
		$access_data = $this->getToken();
		if ($access_data) {
			$vk_api_query = http_build_query([
				'user_ids' => $access_data['user_id'],
				'fields' => 'photo_200',
				'access_token' => $access_data['token']
			]);

			return json_decode(file_get_contents("https://api.vk.com/method/users.get?{$vk_api_query}"))->response[0];
		}else {
			return false;
		}
	}

	public function authenticate(Request $request)
	{
		$this->request = $request;
		$this->vk_user = $this->getUserData();

		if ($this->vk_user) {
			try {
				$user = User::create([
					'vk_id' => $this->vk_user->uid,
					'vk_full_name' => $this->vk_user->first_name . ' ' . $this->vk_user->last_name,
					'vk_photo' => $this->vk_user->photo_200
				]);
			}catch (QueryException $e) {
				$user = User::where('vk_id', $this->vk_user->uid)->first();
			}

			return redirect('reviews')->withCookie(cookie('user', $user, 60));
		}

		return redirect('reviews')->with('status', 'Неудалось авторизоваться в социальной сети');
	}
}