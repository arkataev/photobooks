<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

abstract class SocialAuthController extends AuthController
{
	abstract protected function authorizeAPI();

	abstract protected function getUserData();

	abstract function authenticate(Request $request);
}
