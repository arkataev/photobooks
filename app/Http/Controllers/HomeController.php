<?php

namespace app\Http\Controllers;

use App\Article;
use App\Banner;
use App\Project;
use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
	public function index() {
		
		return view('home', [
			// Random projects (random items loaded on every page load)
			'projects' => Project::where('featured', 0)->orderByRaw("RAND()")->take(4)->get(),
			// Featured Static Project
			'featured' => Project::where('featured', 1)->first(),
			// Banners
			'banners' => Banner::all(),
			// Reviews
			'reviews' => Review::where('approved', 1)->get(),
			// Articles
			'articles' => Article::where('news', 0)->take(2)->get(),
			// Latest News
			'news' => Article::where('news', 1)->orderBy('date_add', 'desc')->first()
		]);
	}

}