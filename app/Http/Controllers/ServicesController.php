<?php

namespace App\Http\Controllers;

use App\Image;
use App\Service;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Project;

class ServicesController extends Controller
{
	/**
	 * Отображает страницу выбранной по ID услуги
	 * Добавляет также на страницу три случайных проекта в качестве примеров
	 * 
	 * @param $id int   ид услуги
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getService($id)
	{
		// Get 3 random projects to show on service page as examples
		$examples = Project::whereHas('categories', function ($query) use ($id) {
			return $query->where('parent_id', '=', $id);
		})->orderByRaw("RAND()")->take(3)->get();

		return view('service', [
			'service' => Service::where('id', $id)->first(),
			'examples' => $examples,
		]);
	}


}
