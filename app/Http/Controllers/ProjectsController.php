<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Project;

class ProjectsController extends Controller
{
	
    public function index($category=1)
    {
	    // Get paginated (by 16 items) projects where category_id is a child of a root $category
	    // Docs on WhereHas (https://laravel.com/docs/5.1/eloquent-relationships#querying-relations)
	    $projects = Project::whereHas('categories', function ($query) use ($category) {
		    return $query->where('parent_id', '=', $category);
	    })->orderBy('date_add', 'desc')->paginate(12);

		return view('projects', [
			'projects' => $projects,
			// Get root categories (Фотосессии, Фотоальбомы...)
			'root' => Categorie::where('parent_id', null)->get(),
			// Get children subcategories (Персональная, Школьная...)
			'children' => Categorie::where('parent_id', $category)->get(),
			'current' => $category
		]);
    }
}
