<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\AuthController;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReviewsController extends Controller
{
	
    public function index(Request $request)
    {
	    return view('reviews', [
		    // Все одобренные модератором отзывы
		    'reviews' => Review::where('approved', 1)->orderBy('date_add', 'desc')->paginate(5),
		    // Данные авторизованного пользователя, сохраненные в куки
		    'user' => $request->cookie('user') ? $request->cookie('user') : null,
	    ]);

    }

	/**
	 * Позволяет авторизованным пользователям оставлять отзывы
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postReview(Request $request)
	{
		if ($request->cookie('user')) {
			$validator = Validator::make($request->all(), ['review_text' => 'required|max:255']);
			if ($validator->fails()) { return redirect('reviews'); }
			Review::insert([
				'user_id' => $request->input('id'),
				'review_text' => $request->input('review_text'),
				'date_add' => date('Y-m-d H:i:s'),
			]);

			return redirect('reviews')->with('status', 'Благодарим за отзыв! Он будет доступен после проверки модератором');

		}else {
			return redirect('reviews')->with('status', 'Возникла ошибка. Попробуйте позже');
		}
	}

	/**
	 * Авторизует пользователя выбранным способом
	 *
	 * @param string $auth_id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function authenticate(string $auth_id)
	{
		$auth_controller = AuthController::getInstance($auth_id);

		return $auth_controller->authorizeAPI();
	}
}
