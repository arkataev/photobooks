<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminLoginController extends Controller
{
	private $authorizer;

	public function __construct()
	{
		$this->authorizer = AuthController::getInstance();
	}

	public function login(Request $request)
	{
		$valid = $this->authorizer->login($request);

		return $valid ? redirect()->intended('admin/banners') : redirect()->back();
	}

	public function logout()
	{
		$this->authorizer->logout();

		return redirect('admin/login');
	}
}
