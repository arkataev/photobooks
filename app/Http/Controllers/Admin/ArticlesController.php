<?php

namespace app\Http\Controllers\Admin;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\File;
use App\Http\Requests\ArticlePostRequest;

class ArticlesController extends Controller
{

	public function index()
	{
		return view('admin.articles', [
			'articles' => Article::orderBy('date_add', 'desc')->paginate(9)
		]);
	}

	
	public function getArticle(Request $request)
	{
		return response()->view('admin.article_form', [
			'article' => Article::where('id', $request->input('id'))->first()
		]);
	}
	
	
	/**
	 * Creates new article entity
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postArticle(ArticlePostRequest $request)
	{
		$data = [
			'title' => $request->input('title'),
			'description' => $request->input('description'),
			'news' => ($request->input('is_news') === 'true'), // convert string to boolean
			'date_add' => date('Y-m-d H:i:s'),
		];

		$article_id = $request->input('id') == 'undefined' ? null : $request->input('id');
		
		if ($article_id) {
			Article::where('id', '=', $article_id)->update($data);
		}else {
			// create new article entry and return id
			$article_id = Article::insertGetId($data, 'id');
		}

		if ($request->hasFile('image_upload')) {
			// create stored file name
			$file_name = Article::getImageName($article_id);
			// get file data from request object
			$file = $request->file('image_upload');
			//  move file to img folder
			$file->move('img/articles', $file_name . '.jpg');
			// create image thumb 684px wide
			$this->resizeImage(684, 500, 'img/articles/'. $file_name);
		}

		return response()->json(['message' => 'Информация обновлена'], 200);
	}

	

	public function deleteArticle(Request $request)
	{
		$article_id = $request->input('id');
		$article_img = 'img/articles/'. Article::getImageName($article_id) . '.jpg';
		$article_img_thumb = 'img/articles/'. Article::getImageName($article_id) . '_rs.jpg' ;
		
		Article::where('id','=', $article_id)->delete();
		File::delete($article_img, $article_img_thumb);

		return response()->json(['message' => 'Запись удалена'], 200);
	}
	
	
}