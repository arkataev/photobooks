<?php

// TODO:: Хранение и загрузка файлов в Ya.disk
// https://laravel.com/docs/5.2/filesystem#custom-filesystems


namespace App\Http\Controllers\Admin;

use App\Categorie;
use App\Image;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectPostRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class ProjectsController extends Controller
{


    public function index()
    {
	    return view('admin.projects', 
		    [
			    'projects'=> Project::paginate(12),
			    'categories' => Categorie::where('parent_id', null)->get(),
		    ]);
    }
	
	
	public function getProject(Request $request)
	{
		$project = Project::where('id', $request->input('id'))->first();
		$types = $project ? Categorie::where('parent_id', $project->category->id)->get() : null;
		
		return response()->view('admin.project_form',
			[
				'project' =>  $project,
				'categories' => Categorie::where('parent_id', null)->get(),
				'types' => $types,
			]);
	}
	
	
	public function getProjectCategory(Request $request)
	{
		return response()->json([
			'type' => isset($request->id) ? Categorie::where('parent_id', $request->input('id'))->get() : null,
		]);
	}
	
	
	/**
	 * Создает новую запись в БД таблице Проекты
	 * Валидация происходит в теле запроса ProjecPostRequest
	 * 
	 * @param ProjectPostRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postProject(ProjectPostRequest $request)
	{
		$data = $request->all();
		$thumbs = $request->allFiles();
		// Создать новую запись проекта и вернуть id
		$project_id = Project::insertGetId([
			'header_1' => $data['header_1'],
			'header_2' => $data['header_2'],
			'description' => $data['description'],
			'category_id' => $data['category_id'],
			'featured' => ($request->input('featured') === 'true'),
			'date_add' => date('Y-m-d H:i:s'),
		]);
		// Сохранить загруженные изображения и создать их миниатюры
		foreach ($thumbs as $file) {
			// достаем имя файла
			$file_name = str_replace(['.' , ' '], '_', $file->getClientOriginalName());
			// определяем данные для файла с данным именем
			$content = json_decode($data[$file_name], true);
			// Создать запись в БД и вернуть id
			$image_id = Image::insertGetId(['route' => 'projects', 'project_id' => $project_id, 'main' => $content['main']]);
			// Переместить загруженное изображение в папку проекта
			$file->move('img/projects/'. $project_id .'/', 'image_' . $image_id . '.jpg');
			// Создать миниатюру
			$this->cropImage($content['width'], $content['height'], $content['x1'], $content['y1'], $content['wr'], $content['hr'], 'img/projects/'. $project_id .'/image_' . $image_id);
		}

		return response()->json(['message' => 'Информация обновлена'], 200);
	}



	public function updateProject(ProjectPostRequest $request)
	{
		$data = $request->all();
		$thumbs = $request->allFiles();

		Project::where('id', $data['id'])->update([
			'header_1' => $data['header_1'],
			'header_2' => $data['header_2'],
			'description' => $data['description'],
			'category_id' => $data['category_id'],
			'featured' => ($request->input('featured') === 'true')
		]);

		// Сохранить загруженные изображения и создать их миниатюры
		foreach ($thumbs as $file) {
			// достаем имя файла
			$file_name = str_replace(['.' , ' '], '_', $file->getClientOriginalName());
			// определяем данные для файла с данным именем
			$content = json_decode($data[$file_name], true);
			// Создать запись в БД и вернуть id
			$image_id = Image::insertGetId(['route' => 'projects', 'project_id' => $data['id'], 'main' => $content['main']]);
			// Переместить загруженное изображение в папку проекта
			$file->move('img/projects/'. $data['id'].'/', 'image_' . $image_id . '.jpg');
			// Создать миниатюру
			$this->cropImage($content['width'], $content['height'], $content['x1'], $content['y1'], $content['wr'], $content['hr'], 'img/projects/'. $data['id'] .'/image_' . $image_id);
		}

		// удаляем изображения
		$images_to_delete = array_filter($data, function($key){
			return strpos($key, 'delete') > -1;
		}, ARRAY_FILTER_USE_KEY);
		
		if ($images_to_delete) {
			foreach ($images_to_delete as $name => $id) {
				$image = 'img/projects/' . $data['id'] . '/image_' . $id . '.jpg';
				$thumb = 'img/projects/' . $data['id'] . '/image_' . $id . '_cr.jpg';
				unlink($image);
				unlink($thumb);
				File::delete($image, $thumb);
				Image::where('id', $id)->delete();
			}
		}

		return response()->json(['message' => 'Информация обновлена'], 200);
	}



	public function deleteProject(Request $request)
	{
		// удаляем запись проект в БД
		Project::where('id', $request->input('id'))->delete();
		// удаляем записи изображений проекта из БД
		Image::where('project_id', $request->input('id'))->delete();
		// Удаляем файлы проекта
		File::deleteDirectory('img/projects/' . $request->input('id'));

		return response()->json([
			'message' => 'Проект успешно удален',
			'type' => 'success',
		]);
	}


	protected function saveImage($image)
	{
		
	}
}
