<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 19.06.2016
 */

namespace app\Http\Controllers\Admin;

use App\Banner;

use App\Http\Controllers\Controller;
use App\Http\Requests\BannerUpdateRequest;


class BannersController extends Controller
{

	public function index()
	{
		return view('admin.banners', [
			'banners' => Banner::all(),
		]);
	}
	
	
	public function updateBanner(BannerUpdateRequest $request)
	{
		$data = [
			'header_1' => $request->input('header_1'),
			'header_2' => $request->input('header_2'),
			'target_url' => $request->input('target_url'),
		];
		$file_name = 'banner' . '_' . $request->input('id') . '.jpg';
		$request->hasFile('image_upload') ? $request->file('image_upload')->move('img/banners', $file_name) : false;
		Banner::where('id', $request->input('id'))->update($data);
		
		return response()->json(['message' => 'Информация успешно обновлена'], 200);
	}
	
}