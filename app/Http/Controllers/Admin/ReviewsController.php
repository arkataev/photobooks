<?php

namespace App\Http\Controllers\Admin;

use App\Review;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReviewsController extends Controller
{
	
	
    public function index()
    {
	    return view('admin.reviews', [
		    'reviews' => Review::orderBy('date_add', 'desc')->paginate(10),
	    ]);
    }
	
	
	public function approveReview(Request $request)
	{
		Review::where('id', $request->input('id'))->update(['approved' => 1]);
	}
	
	public function denyReview(Request $request)
	{
		Review::where('id', $request->input('id'))->update(['approved' => 0]);
	}
}
