<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImagesController extends Controller
{
	public function index()
	{
		return view('admin.gallery', [
			'images' => Image::where('project_id', 0)->get(),
		]);
	}


	public function postImage(Request $request)
	{
		$data = $request->all();    // accept thumbnail crop dimensions
		$file = $request->file('image_upload');
		$image_id = Image::insertGetId(['route' => 'gallery', 'project_id' => 0]);
		$file->move('img/gallery', 'image_' . $image_id . '.jpg');
		// Create thumbnail
		$this->cropImage($data['width'], $data['height'], $data['x1'], $data['y1'], $data['wr'], $data['hr'], 'img/gallery/image_'.$image_id);

		return response()->json([
			'message' => 'Информация обновлена',
			'type' => 'success',
		]);
	}


	public function deleteImage(Request $request)
	{
		$image_id = $request->input('id');
		// Достаем данные изображение из БД
		$target  = Image::where('id','=', $image_id)->first();
		// определяем имена кандидатов на удаление
		$image = 'img/'. $target->route .'/image_'. $target->id . '.jpg';       // большое изображение
		$image_thumb = 'img/' . $target->route . '/image_'. $target->id . '_cr.jpg';  // миниатюра
		// удаляем запись из БД
		Image::where('id','=', $image_id)->delete();
		// удаляем файлы с диска
		if (file_exists($image)) {
			unlink($image);
		}

		if (file_exists($image_thumb)) {
			unlink($image_thumb);
		}

		return response()->json([
			'message' => 'Информация обновлена',
			'type' => 'success',
		]);
	}
}
