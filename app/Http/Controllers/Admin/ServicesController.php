<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 23.06.2016
 */

namespace app\Http\Controllers\Admin;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class ServicesController extends Controller
{
	
	public function index()
	{
		return view('admin.services', [
			'services' => Service::all()
		]);
	}
	
	
	public function updateService(Request $request)
	{
		Service::where('id', $request->input('id'))->update([
			'description' => $request->input('description'),
		]);
		
		return response()->json(['message' => 'Информация обновлена']);
	}
	
}