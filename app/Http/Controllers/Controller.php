<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Mockery\CountValidator\Exception;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    
    /**
     *  Creates resized .jpeg image with preserved proportions
     *  Image will be resized by width or heigth, depending on what unit measure is bigger 
     *  if image width is bigger, than provided $max_width will be used and image height will be changed
     *  else $max_height will be used
     *
     * @param $max_width    int     output image_thumb width
     * @param $max_height   int     output image_thumb height
     * @param $file_name    string  route to save image
     * 
     * @return bool
     */
    protected function resizeImage($max_width, $max_height, $file_name)
    {
        $img = getimagesize($file_name . '.jpg');
        list($orig_width, $orig_height) = $img;

        $orig_ratio = $orig_width/$orig_height;

        if ($max_width/$max_height > $orig_ratio) {
            $max_width = $max_height*$orig_ratio;
        } else {
            $max_height = $max_width/$orig_ratio;
        }

        $image_p = imagecreatetruecolor($max_width, $max_height);
        
        if (exif_imagetype($file_name . '.jpg') == IMAGETYPE_JPEG) {
            $image = imagecreatefromjpeg($file_name . '.jpg');
        }elseif(exif_imagetype($file_name . '.jpg') == IMAGETYPE_PNG) {
            $image = imagecreatefrompng($file_name . '.jpg');
        }
        
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, intval($max_width), intval($max_height), $orig_width, $orig_height);

        return imagejpeg($image_p, $file_name . '_rs.jpg', 100);
    }

    /**
     * Crops uploaded image and saves new .jpeg image thumbnail
     * 
     * @param $max_width    int     Width of cropped area
     * @param $max_height   int     Height of cropped area
     * @param $x1           int     Start X position of cropped area
     * @param $y1           int     Start Y position of cropped area
     * @param $wr           float   Ratio of original image width and displayed css block width
     * @param $hr           float   Ration of original image height and displayed css block height
     * @param $file_name    string  Route to save image
     * 
     * @return bool
     */
    protected function cropImage($max_width, $max_height, $x1, $y1, $wr, $hr, $file_name)
    {

        $image_p = imagecreatetruecolor(800, 800);

        if (exif_imagetype($file_name . '.jpg') == IMAGETYPE_JPEG) {
            $image = imagecreatefromjpeg($file_name . '.jpg');
        }elseif(exif_imagetype($file_name . '.jpg') == IMAGETYPE_PNG) {
            $image = imagecreatefrompng($file_name . '.jpg');
        }

        imagecopyresampled($image_p, $image, 0, 0, intval($x1) * $wr, intval($y1) * $hr, intval(800), intval(800), intval($max_width) * $wr, intval($max_height) * $hr);

        return imagejpeg($image_p, $file_name . '_cr.jpg', 100);
    }
}
