<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


class ArticlePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
            'description' => 'required',
            'image_upload' => 'image|dimensions:min_width=1900,min-height=700|mimes:jpeg,png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'image_upload.dimensions' => 'Неверный размер изображения',
            'image_upload.mimes' => 'Неверный формат изображения',
            'image_upload.image' => 'Необходимо загрузить изображение',
            'title.required' => 'Необходимо указать заголовок',
            'description.required' => 'Необходимо указать содержание'
        ];
    }

    public function validate()
    {
        // проверяем есть ли файл в загрузке или на диске
        $file =  $this->file('image_upload') ? $this->file('image_upload') : File::exists('img/articles/article_img_' . $this->input('id') . '.jpg');

        if (!$file) {
            throw new FileException('Необходимо выбрать изображение');
        }else if (gettype($file) == 'object') {
            // если загружается новый файл - проверить его
            if (!$file->isValid() || $file->getClientSize() > 2000000) {
                throw new FileException('Возникла ошибка при загрузке файла. Проверьте размер и тип изображения. Размер файла не должен превышать 2МБ');
            }
        }

        $validator = Validator::make($this->all(), $this->rules(), $this->messages());
        // Если есть ошибки бросить ошибку валидации
        $validator->fails() ? $this->failedValidation($validator) : true ;
    }
}
