<?php

namespace App\Http\Requests;

use App\Image;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ProjectPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->all();
        $files = $this->allFiles();
        $rules = [
            'header_1' => 'required|max:50',
            'header_2' => 'required|max:100',
            'description' => 'required',
            'category_id' => 'required',
            'featured' => 'unique_featured'
        ];
        
        foreach ($files as $key => $file) {
            $file_name = str_replace(['.' , ' '], '_', $file->getClientOriginalName());
            $content = json_decode($data[$file_name], true);
            if ($content['main']) {
                $rules[$key] = 'image|dimensions:min_width=1900,min_height=1000|mimes:jpeg,png,jpg';
            }else {
                $rules[$key] = 'image|mimes:jpeg,png,jpg';
            }
        }

        return $rules;
    }


    /**
     * Возвращает массив пользовательских сообщений для подстановки
     * в сообщения об ошибках
     * 
     * @return array
     */
    public function messages()
    {
        $files = $this->allFiles();
        $messages = [
            'header_1.required' => 'Не указано название проекта',
            'header_2.required' => 'Не указано краткое описание проекта',
            'description.required' => 'Не указано описание проекта',
            'category_id.required' => 'Необходимо выбрать категорию проекта',
            'featured.unique_featured' => 'Избранный проект уже существует'
        ];

        foreach ($files as $name => $file) {
            $messages[$name . '.dimensions'] = 'Неверный размер изображения';
            $messages[$name . '.mimes'] = 'Неверный формат изображения';
            $messages[$name . '.image'] = 'Необходимо загрузить изображение';
        }

        return $messages;
    }

    /**
     *  Проверяет наличие и параметры необходимых изображений в принятом запросе на постинг
     *  нового проекта, а также наличие и содержание обязательных полей с описанием проекта
     */
    public function validate()
    {
        $files = $this->allFiles();
        $data = $this->all();
        // проверить есть ли в базе данных главное изображение для проекта
        $image = Image::where([['project_id', $this->input('id')],['main', 1]])->first();
        // проверить есть ли главное изображение в загружаемых файлах
        $main_image_uploaded = array_filter($files, function ($file) use ($data) {
            $file_name = str_replace(['.' , ' '], '_', $file->getClientOriginalName());
            return json_decode($data[$file_name], true)['main'];
        });
        // получаем id изобржений на удаление из БД
        $images_to_delete = array_filter($data, function($key){
            return strpos($key, 'delete') > -1;
        }, ARRAY_FILTER_USE_KEY);

        // если в базе есть главное изображение проверить есть ли его id в массиве на удаление
        if ($image) {
            $main_image = in_array($image->id, array_values($images_to_delete)) ? false : true;
            $main_image = $main_image ? true :  $main_image_uploaded ;
        }else {
            $main_image = $main_image_uploaded;
        }

            // проверить валидность загружаемых файлов, включая ограничение на маскимальный размер файла (2МБ)
        if (!empty(array_filter($files, function ($file) { return !$file->isValid() ? true : false; }))) {
            throw new FileException('Возникла ошибка при загрузке файлов. Проверьте размер и тип файлов');
            // Проверить назначено ли главное изображение проекту
        }elseif (!$main_image) {
            throw new FileException('Невыбрано главное изображение проекта');
        }else {
            // Проверить остальные поля формы
            $validator = Validator::make($this->all(), $this->rules(), $this->messages());
            // Если есть ошибки бросить ошибку валидации
            $validator->fails() ? $this->failedValidation($validator) : null;
        }
    }
}
