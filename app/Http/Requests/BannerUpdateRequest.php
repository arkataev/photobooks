<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class BannerUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'header_1' => 'required|max:50',
            'header_2' => 'required|max:100',
            'target_url' => 'required|url',
            'image_upload' => 'sometimes|image|dimensions:min_width=1900,height=700|mimes:jpeg,png,jpg'
        ];
    }
    
    public function messages()
    {
        return [
            'image_upload.dimensions' => 'Неверный размер изображения',
            'image_upload.mimes' => 'Неверный формат изображения',
            'image_upload.image' => 'Необходимо загрузить изображение',
            'header_1.required' => 'Необходимо указать заголовок',
            'header_2.required' => 'Необходимо указать описание',
            'target_url.url' => 'Необходимо указать правильную ссылку',
            'target_url.required' => 'Необходимо указать правильную ссылку'
        ];
    }
    
    
    public function validate()
    {
        // проверяем есть ли файл в загрузке или на диске
        $file =  $this->file('image_upload') ? $this->file('image_upload') : File::exists('img/banners/banner_' . $this->input('id') . '.jpg');

        if (!$file) {
            throw new FileException('Необходимо выбрать изображение');
        }else if (gettype($file) == 'object') {
            // если загружается новый файл - проверить его
            if (!$file->isValid() || $file->getClientSize() > 2000000) {
                throw new FileException('Возникла ошибка при загрузке файла. Проверьте размер и тип изображения. Размер файла не должен превышать 2МБ');
            }
        }

        $validator = Validator::make($this->all(), $this->rules(), $this->messages());
        // Если есть ошибки бросить ошибку валидации
        $validator->fails() ? $this->failedValidation($validator) : true ;
    }
}
