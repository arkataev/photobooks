<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public static $messages = [
	    'title.required' => 'Необходимо заполнить Заголовок',
	    'description.required' => 'Необходимо заполнить Описание',
    ];

	public static function getImageName($id)
	{
		return 'article_img_' . $id;
	}
}
